module kredUV ( clk, resetn, inU, inV, outUpV, outUmV );

    //parameter INU_SIZE = 16;
    //parameter INV_SIZE = 30;

    input               clk;
	input               resetn;
    input       [15:0]  inU;
    input       [31:0]  inV;
    output reg  [15:0]  outUpV;
    output reg  [15:0]  outUmV;

    wire        [15:0]  sU0;        // U0
    wire        [15:0]  dU0;        // 2*U0
    wire        [15:0]  sU1;        // U1

    wire        [15:0]  sV0;        // V0
    wire        [15:0]  oV0;        // 8*V0
    wire        [15:0]  sV1;        // V1
    wire        [15:0]  dV1;        // 2*V1
    wire        [15:0]  sV2;        // V2

    reg         [15:0]  tU0;        // 3*U0
    reg         [15:0]  nV0;        // 9*V0 
    reg         [15:0]  tV1;        // 3*V1
    reg         [15:0]  U1pV2;      // U1 + V2
    reg         [15:0]  U1mV2;      // U1 - V2

    reg         [15:0]  nV0mtV1;    // 9V0 - 3V1
    reg         [15:0]  tU0mU1pV2;  // 3U0 - U1 + V2
    reg         [15:0]  tU0mU1mV2;  // 3U0 - U1 - V2

    reg         inV_s0;//inV[11] clk - 1
    reg         inV_s1;//inV[11] clk - 2

    assign sU0 = { 4'b0, inU[11:0] };
    assign dU0 = { 3'b0, inU[11:0],  1'b0 };

    assign sU1 = { 12'b0, inU[15:12] };////the original version has sign extension now we dont { {12{inU[15]}}, inU[15:12] };

    assign sV0 = { 4'b0, inV[11:0]        };
    assign oV0 = { 1'b0, inV[11:0],  3'b0 };

    assign sV1 = { 4'b0, inV[23:12]       };
    assign dV1 = { 3'b0, inV[23:12], 1'b0 };

    assign sV2 = { 10'b0, inV[31:24] };////the original version has sign extension now we dont { {9{inV[30]}}, inV[30:24] };

    always @ ( posedge clk ) 
        begin
        if (resetn == 1'b0)
            begin
            tU0 <= 0;
            nV0 <= 0;
            tV1 <= 0;
            U1mV2 <= 0;
            U1pV2 <= 0;
            inV_s0 <= 0;

            nV0mtV1 <= 0;
            tU0mU1pV2 <= 0;
            tU0mU1mV2 <= 0;
            inV_s1 <= 0;

            outUpV <= 0;
            outUmV <= 0;
            
            end
        else
            begin
            tU0 <= dU0 + sU0;
            nV0 <= oV0 + sV0;
            tV1 <= dV1 + sV1;
            U1mV2 <= sU1 - sV2;
            U1pV2 <= sU1 + sV2;
            inV_s0 <= inV[11];

            nV0mtV1 <= nV0 - tV1;
            tU0mU1pV2 <= tU0 - U1mV2;
            tU0mU1mV2 <= tU0 - U1pV2;
            inV_s1 <= inV_s0;

            //change to inV1 in order to fit the data currently process////might need another stage
            outUpV <= (inV_s1) ? tU0mU1pV2 + nV0mtV1 : tU0mU1pV2 + nV0mtV1 + 16'd24578;//depend on V0 add 2q accordingly
            outUmV <= (inV_s1) ? tU0mU1mV2 - nV0mtV1 + 16'd49156 : tU0mU1mV2 - nV0mtV1 + 16'd24578; //depend on V0 add 2q or 4q accordingly
            end
        end

endmodule


