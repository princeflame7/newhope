`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:   
// Design Name: 
// Module Name:    NTT_1024_Mod12289_kred(v2)
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: reduce register usage (Coding style)
//						Add pipeline
//
//////////////////////////////////////////////////////////////////////////////////
module NTT_1024_Mod12289_kred(
	Clk,
	Resetn,
	Cmd,
	Addr,
	DI,
	DO,
	Valid
    );

parameter P_WIDTH = 16;
parameter W_WIDTH = 14; ////for w and phi
parameter N = 8;
////State
parameter INIT          = 0;
parameter PHI_A    		= 1;
parameter ORDER_REV		= 2;
parameter FFT_A        	= 3;
parameter FIN			= 4;
////Cmd
parameter WRITE			= 1;  ////Directly write coefficient to bram a0
parameter START_NTT		= 2;
parameter START_INTT	= 3;
parameter RESET 		= 4;

input 							Clk;
input							Resetn;
input 		[2 : 0] 			Cmd;
input		[15 : 0] 			Addr;
input		[P_WIDTH*8 -1 : 0]	DI;
output		[P_WIDTH*8 -1 : 0]	DO;
output					Valid; ////Result able to be read

wire							Valid;
assign 							Valid = (Cmd == START_NTT) ? FFT_Fin : PHI_Fin;

reg 			[3 : 0]			stage;
reg				[3 : 0]			stage_next;
integer 		i;

////Module Port
wire 		[P_WIDTH-1 : 0]		a 		[0 : N-1];
wire		[W_WIDTH-1 : 0]		phi		[0 : N-1];

reg 							a0_we;
reg 		[7 : 0]				a0_addr [0 : 1];
reg 		[P_WIDTH*4 -1 : 0]	a0_di 	[0 : 1];
wire 		[P_WIDTH*4 -1 : 0]	a0_do 	[0 : 1];

reg 							a1_we;
reg 		[7 : 0]				a1_addr [0 : 1];
reg 		[P_WIDTH*4 -1 : 0]	a1_di 	[0 : 1];
wire 		[P_WIDTH*4 -1 : 0]	a1_do 	[0 : 1];


wire 		[7 : 0]				p_addr 	[0 : 1];
wire 		[W_WIDTH*4 -1 : 0]	p_do 	[0 : 1];

reg								Mod_0_en;
reg         [P_WIDTH+W_WIDTH-1 : 0]    	Mod_0_in    [0 : N-1];
wire        [P_WIDTH-1 : 0]     Mod_0_out	[0 : N-1];
wire							Mod_0_Vld	[0 : N-1];
wire							Mod_0_Complete;

wire	 	[W_WIDTH-1 : 0]		BFU_w		[0 : N/2-1];
wire	 	[P_WIDTH-1 : 0]		BFU_in		[0 : N-1];
wire	 	[P_WIDTH-1 : 0]		BFU_out		[0 : N-1];
wire							BFU_done;

reg 		[7 : 0]				w_addr 		[0 : 1];
wire 		[W_WIDTH*2 -1 : 0]	w_do 		[0 : 1];


wire 		[W_WIDTH*4 -1 : 0]	pi_do 		[0 : 1];
wire 		[W_WIDTH*2 -1 : 0]	wi_do 		[0 : 1];

////Stage Reg Wire Declaration
reg 							read_ram;
wire 							write_ram;
reg			[8 : 0]				read_addr 		[0 : 1];
reg			[8 : 0]				read_addr_next 	[0 : 1];
reg			[8 : 0]				write_addr 		[0 : 1];
reg			[8 : 0]				write_addr_next	[0 : 1];


wire							PHI_Fin;
wire							ORD_Fin;
wire							FFT_Fin;

reg 		[8 : 0]				cnt;//General counter

wire		[5 : 0]				order_reverse_tail  [0 : 1];
wire		[7 : 0]				order_reverse_addr  [0 : N-1];
reg 		[P_WIDTH*4 -1 : 0]	order_reverse_data  [0 : N-1];

wire		[7 : 0]				fft_write_addr[0 : 1];
reg			[3 : 0]				fft_stage;
wire							fft_stage_done;

////Module Declaration
//Starting with a0 can use bram_dual_init for simulation testing 
bram_dual_kred 	a0(.Clk(Clk), .En(1'b1), .We(a0_we), .Addr_A(a0_addr[0]), .Addr_B(a0_addr[1]), .DI_A(a0_di[0]), .DI_B(a0_di[1]), .DO_A(a0_do[0]), .DO_B(a0_do[1]));
bram_dual_kred 	a1(.Clk(Clk), .En(1'b1), .We(a1_we), .Addr_A(a1_addr[0]), .Addr_B(a1_addr[1]), .DI_A(a1_di[0]), .DI_B(a1_di[1]), .DO_A(a1_do[0]), .DO_B(a1_do[1]));
phi_bram_dual_kred 	p(.Clk(Clk), .En(1'b1), .We(1'b0), .Addr_A(p_addr[0]), .Addr_B(p_addr[1]), .DI_A(56'd0), .DI_B(56'd0), .DO_A(p_do[0]), .DO_B(p_do[1]));
w_bram_dual_kred 	w(.Clk(Clk), .En(1'b1), .We(1'b0), .Addr_A(w_addr[0]), .Addr_B(w_addr[1]), .DI_A(28'd0), .DI_B(28'd0), .DO_A(w_do[0]), .DO_B(w_do[1]));

phii_bram_dual_kred pi (.Clk(Clk), .En(1'b1), .We(1'b0), .Addr_A(p_addr[0]), .Addr_B(p_addr[1]), .DI_A(56'd0), .DI_B(56'd0), .DO_A(pi_do[0]), .DO_B(pi_do[1]));
wi_bram_dual_kred 	wi (.Clk(Clk), .En(1'b1), .We(1'b0), .Addr_A(w_addr[0]), .Addr_B(w_addr[1]), .DI_A(28'd0), .DI_B(28'd0), .DO_A(wi_do[0]), .DO_B(wi_do[1]));


BFUset_8_mod12289 BFUset0( 
	.Clk(Clk),
	.Resetn(Resetn),	
	.w0(BFU_w[0]), .w1(BFU_w[1]), .w2(BFU_w[2]), .w3(BFU_w[3]),
	.a0(BFU_in[0]), .a1(BFU_in[1]), .a2(BFU_in[2]), .a3(BFU_in[3]), .a4(BFU_in[4]), .a5(BFU_in[5]), .a6(BFU_in[6]), .a7(BFU_in[7]),
	.A0(BFU_out[0]), .A1(BFU_out[1]), .A2(BFU_out[2]), .A3(BFU_out[3]), .A4(BFU_out[4]), .A5(BFU_out[5]), .A6(BFU_out[6]), .A7(BFU_out[7])
    );

kredUV Mod_0_0 (.clk(Clk), .resetn(Resetn),  .inU(16'd0), .inV({2'd0, Mod_0_in[0]}), .outUpV(Mod_0_out[0]), .outUmV());
kredUV Mod_0_1 (.clk(Clk), .resetn(Resetn),  .inU(16'd0), .inV({2'd0, Mod_0_in[1]}), .outUpV(Mod_0_out[1]), .outUmV());
kredUV Mod_0_2 (.clk(Clk), .resetn(Resetn),  .inU(16'd0), .inV({2'd0, Mod_0_in[2]}), .outUpV(Mod_0_out[2]), .outUmV());
kredUV Mod_0_3 (.clk(Clk), .resetn(Resetn),  .inU(16'd0), .inV({2'd0, Mod_0_in[3]}), .outUpV(Mod_0_out[3]), .outUmV());
kredUV Mod_0_4 (.clk(Clk), .resetn(Resetn),  .inU(16'd0), .inV({2'd0, Mod_0_in[4]}), .outUpV(Mod_0_out[4]), .outUmV());
kredUV Mod_0_5 (.clk(Clk), .resetn(Resetn),  .inU(16'd0), .inV({2'd0, Mod_0_in[5]}), .outUpV(Mod_0_out[5]), .outUmV());
kredUV Mod_0_6 (.clk(Clk), .resetn(Resetn),  .inU(16'd0), .inV({2'd0, Mod_0_in[6]}), .outUpV(Mod_0_out[6]), .outUmV());
kredUV Mod_0_7 (.clk(Clk), .resetn(Resetn),  .inU(16'd0), .inV({2'd0, Mod_0_in[7]}), .outUpV(Mod_0_out[7]), .outUmV());

////State Machine
always @ (posedge Clk)
	if (Resetn == 1'b0)
		stage <= INIT;
	else
		stage <= stage_next;

always @ (*)
	case (stage)
		INIT	: 
			begin
			if (Cmd == START_NTT)
				stage_next = PHI_A;
			else if (Cmd == START_INTT)
				stage_next = ORDER_REV;
			else 
				stage_next = INIT;
			end
		PHI_A	:
			begin
			if (PHI_Fin && Cmd == START_NTT)
				stage_next = ORDER_REV;
			else if (PHI_Fin && Cmd == START_INTT)
				stage_next = FIN;
			else
				stage_next = PHI_A;
			end
		ORDER_REV :
			begin
			if (ORD_Fin)
				stage_next = FFT_A;
			else
				stage_next = ORDER_REV;
			end
		FFT_A :
			begin
			if (FFT_Fin && Cmd == START_NTT)
				stage_next = FIN;
			else if (FFT_Fin && Cmd == START_INTT)
				stage_next = PHI_A;
			else
				stage_next = FFT_A;
			end
		FIN	:
			begin	
			if (Cmd == RESET)
				stage_next = INIT;
			else
				stage_next = FIN;
			end
		default	:	stage_next = stage;
	endcase
	

////General assignment
assign write_ram = ~read_ram;
assign {a[0], a[1], a[2], a[3]} = (read_ram == 1'b0) ? a0_do[0] : a1_do[0];
assign {a[4], a[5], a[6], a[7]} = (read_ram == 1'b0) ? a0_do[1] : a1_do[1];
////assign N^(-1) = 10109; //// 3^(-14) * 1024 ^(-1) ////But already included in phi_i_kred

assign DO = {a0_do[0], a0_do[1]};

////PHI assignment
assign PHI_Fin = (stage == PHI_A) && (write_addr[0] == 254);

assign {phi[0], phi[1], phi[2], phi[3]} = (Cmd == START_NTT) ? p_do[0] : pi_do[0];
assign {phi[4], phi[5], phi[6], phi[7]} = (Cmd == START_NTT) ? p_do[1] : pi_do[1];
assign p_addr[0] = read_addr[0][7 : 0];
assign p_addr[1] = read_addr[1][7 : 0];
assign Mod_0_Complete = (cnt > 3);////3

////ORDER_REVERSE assignment
assign ORD_Fin = (stage == ORDER_REV) &&  (read_addr[0] == 254) && (cnt[2:0] == 7);

assign order_reverse_tail[0] = {read_addr[0][0], read_addr[0][1], read_addr[0][2], read_addr[0][3], read_addr[0][4], read_addr[0][5] };
assign order_reverse_tail[1] = {read_addr[1][0], read_addr[1][1], read_addr[1][2], read_addr[1][3], read_addr[1][4], read_addr[1][5] };

assign order_reverse_addr[0] = {2'b00, order_reverse_tail[0]};
assign order_reverse_addr[1] = {2'b10, order_reverse_tail[0]};
assign order_reverse_addr[2] = {2'b01, order_reverse_tail[0]};
assign order_reverse_addr[3] = {2'b11, order_reverse_tail[0]};
assign order_reverse_addr[4] = {2'b00, order_reverse_tail[1]};
assign order_reverse_addr[5] = {2'b10, order_reverse_tail[1]};
assign order_reverse_addr[6] = {2'b01, order_reverse_tail[1]};
assign order_reverse_addr[7] = {2'b11, order_reverse_tail[1]};

////FFT assignment
assign BFU_done = (cnt > 4);////4
assign fft_stage_done = (write_addr[0] == 254) && (a0_we || a1_we);
assign FFT_Fin = (stage == FFT_A) &&  (fft_stage == 9) && (fft_stage_done == 1'b1);

//address due to butterfly  //write address will append until write is conduct (usually one cycle difference with read)
assign fft_write_addr[0] = {1'b0, write_addr[0][7 : 1]};
assign fft_write_addr[1] = fft_write_addr[0] + 8'd128;

assign BFU_in[0] = a[0];
assign BFU_in[1] = a[1];
assign BFU_in[2] = a[2];
assign BFU_in[3] = a[3];
assign BFU_in[4] = a[4];
assign BFU_in[5] = a[5];
assign BFU_in[6] = a[6];
assign BFU_in[7] = a[7];

assign BFU_w[0] = (Cmd == START_NTT) ? (w_do[0][27:14]) : (wi_do[0][27:14]);
assign BFU_w[1] = (Cmd == START_NTT) ? ((fft_stage == 9) ? w_do[0][13 : 0] : w_do[0][27 : 14]) : ((fft_stage == 9) ? wi_do[0][13 : 0] : wi_do[0][27 : 14]);
assign BFU_w[2] = (Cmd == START_NTT) ? (w_do[1][27:14]) : (wi_do[1][27:14]);
assign BFU_w[3] = (Cmd == START_NTT) ? ((fft_stage == 9) ? w_do[1][13 : 0] : w_do[1][27 : 14]) : ((fft_stage == 9) ? wi_do[1][13 : 0] : wi_do[1][27 : 14]);

////Bram data, address setting
always @ (*)
	begin
	case (stage)
		INIT :
			begin
			a0_addr[0] = Addr[15 : 8];////From input
			a0_addr[1] = Addr[7 : 0];
			a1_addr[0] = 0;//trivial
			a1_addr[1] = 0;//trivial
			end
		PHI_A : 
			begin
			if (Cmd == START_NTT) ////read from 0 write to 1 in NTT
				begin
				a0_addr[0] = read_addr[0][7 : 0];
				a0_addr[1] = read_addr[1][7 : 0];
				a1_addr[0] = write_addr[0][7 : 0];
				a1_addr[1] = write_addr[1][7 : 0];
				end
			else ////read from 1 write to 0 in INTT
				begin
				a0_addr[0] = write_addr[0][7 : 0];
				a0_addr[1] = write_addr[1][7 : 0];
				a1_addr[0] = read_addr[0][7 : 0];
				a1_addr[1] = read_addr[1][7 : 0];
				end
			end
		ORDER_REV : ////NTT read from 1 write to 0
			begin
			if (Cmd == START_NTT)
				begin
				case (cnt[2:0])
					0 :
						begin
						a0_addr[0] = order_reverse_addr[0];
						a0_addr[1] = order_reverse_addr[1];
						a1_addr[0] = read_addr[0][7 : 0];
						a1_addr[1] = read_addr[1][7 : 0];
						end
					1 :
						begin
						a0_addr[0] = order_reverse_addr[0];
						a0_addr[1] = order_reverse_addr[1];
						a1_addr[0] = read_addr[0][7 : 0];
						a1_addr[1] = read_addr[1][7 : 0];
						end
					2 :
						begin
						a0_addr[0] = order_reverse_addr[2];
						a0_addr[1] = order_reverse_addr[3];
						a1_addr[0] = read_addr[0][7 : 0];
						a1_addr[1] = read_addr[1][7 : 0];
						end
					3 :
						begin
						a0_addr[0] = order_reverse_addr[2];
						a0_addr[1] = order_reverse_addr[3];
						a1_addr[0] = read_addr[0][7 : 0];
						a1_addr[1] = read_addr[1][7 : 0];
						end
					4 :
						begin
						a0_addr[0] = order_reverse_addr[4];
						a0_addr[1] = order_reverse_addr[5];
						a1_addr[0] = read_addr[0][7 : 0];
						a1_addr[1] = read_addr[1][7 : 0];
						end
					5 :
						begin
						a0_addr[0] = order_reverse_addr[4];
						a0_addr[1] = order_reverse_addr[5];
						a1_addr[0] = read_addr[0][7 : 0];
						a1_addr[1] = read_addr[1][7 : 0];
						end
					6 :
						begin
						a0_addr[0] = order_reverse_addr[6];
						a0_addr[1] = order_reverse_addr[7];
						a1_addr[0] = read_addr[0][7 : 0];
						a1_addr[1] = read_addr[1][7 : 0];
						end
					7 :
						begin
						a0_addr[0] = order_reverse_addr[6];
						a0_addr[1] = order_reverse_addr[7];
						a1_addr[0] = read_addr[0][7 : 0];
						a1_addr[1] = read_addr[1][7 : 0];
						end
				endcase
				end
			else ////INTT read from 0 write to 1
				begin
				case (cnt[2:0])
					0 :
						begin
						a1_addr[0] = order_reverse_addr[0];
						a1_addr[1] = order_reverse_addr[1];
						a0_addr[0] = read_addr[0][7 : 0];
						a0_addr[1] = read_addr[1][7 : 0];
						end
					1 :
						begin
						a1_addr[0] = order_reverse_addr[0];
						a1_addr[1] = order_reverse_addr[1];
						a0_addr[0] = read_addr[0][7 : 0];
						a0_addr[1] = read_addr[1][7 : 0];
						end
					2 :
						begin
						a1_addr[0] = order_reverse_addr[2];
						a1_addr[1] = order_reverse_addr[3];
						a0_addr[0] = read_addr[0][7 : 0];
						a0_addr[1] = read_addr[1][7 : 0];
						end
					3 :
						begin
						a1_addr[0] = order_reverse_addr[2];
						a1_addr[1] = order_reverse_addr[3];
						a0_addr[0] = read_addr[0][7 : 0];
						a0_addr[1] = read_addr[1][7 : 0];
						end
					4 :
						begin
						a1_addr[0] = order_reverse_addr[4];
						a1_addr[1] = order_reverse_addr[5];
						a0_addr[0] = read_addr[0][7 : 0];
						a0_addr[1] = read_addr[1][7 : 0];
						end
					5 :
						begin
						a1_addr[0] = order_reverse_addr[4];
						a1_addr[1] = order_reverse_addr[5];
						a0_addr[0] = read_addr[0][7 : 0];
						a0_addr[1] = read_addr[1][7 : 0];
						end
					6 :
						begin
						a1_addr[0] = order_reverse_addr[6];
						a1_addr[1] = order_reverse_addr[7];
						a0_addr[0] = read_addr[0][7 : 0];
						a0_addr[1] = read_addr[1][7 : 0];
						end
					7 :
						begin
						a1_addr[0] = order_reverse_addr[6];
						a1_addr[1] = order_reverse_addr[7];
						a0_addr[0] = read_addr[0][7 : 0];
						a0_addr[1] = read_addr[1][7 : 0];
						end
				endcase
				end
			end
		FFT_A :
			begin
			a0_addr[0] = (read_ram == 1'b0) ? read_addr[0][7 : 0] : fft_write_addr[0];
			a0_addr[1] = (read_ram == 1'b0) ? read_addr[1][7 : 0] : fft_write_addr[1];
			a1_addr[0] = (read_ram == 1'b0) ? fft_write_addr[0] : read_addr[0][7 : 0];
			a1_addr[1] = (read_ram == 1'b0) ? fft_write_addr[1] : read_addr[1][7 : 0];
			end
		FIN :
			begin
			a0_addr[0] = Addr[15 : 8];////From input
			a0_addr[1] = Addr[7 : 0];
			a1_addr[0] = 0;//trivial
			a1_addr[1] = 0;//trivial
			end
		default :
			begin
			a0_addr[0] = 0;//trivial
			a0_addr[1] = 0;//trivial
			a1_addr[0] = 0;//trivial
			a1_addr[1] = 0;//trivial
			end
	endcase
	end
//data write	
always @ (*)
	begin
	case (stage)
		INIT :
			begin////initial input data to bram a0
			a0_di[0] = DI[127 : 64];
			a0_di[1] = DI[63  :  0];
			a1_di[0] = 64'd0;//trivial
			a1_di[1] = 64'd0;//trivial
			end
		PHI_A:
			begin
			a0_di[0] = {Mod_0_out[0], Mod_0_out[1], Mod_0_out[2], Mod_0_out[3]};
			a0_di[1] = {Mod_0_out[4], Mod_0_out[5], Mod_0_out[6], Mod_0_out[7]};
			a1_di[0] = {Mod_0_out[0], Mod_0_out[1], Mod_0_out[2], Mod_0_out[3]};
			a1_di[1] = {Mod_0_out[4], Mod_0_out[5], Mod_0_out[6], Mod_0_out[7]};
			end
		ORDER_REV:
			begin
			if (Cmd == START_NTT)
				begin
				case (cnt[2 : 0])
					0 :
						begin
						a0_di[0] = order_reverse_data[0];
						a0_di[1] = order_reverse_data[1];
						a1_di[0] = 64'd0;//trivial
						a1_di[1] = 64'd0;//trivial
						end
					1 :
						begin
						a0_di[0] = order_reverse_data[0];
						a0_di[1] = order_reverse_data[1];
						a1_di[0] = 64'd0;//trivial
						a1_di[1] = 64'd0;//trivial
						end
					2 :
						begin
						a0_di[0] = order_reverse_data[2];
						a0_di[1] = order_reverse_data[3];
						a1_di[0] = 64'd0;//trivial
						a1_di[1] = 64'd0;//trivial
						end	
					3 :
						begin
						a0_di[0] = order_reverse_data[2];
						a0_di[1] = order_reverse_data[3];
						a1_di[0] = 64'd0;//trivial
						a1_di[1] = 64'd0;//trivial
						end
					4 :
						begin
						a0_di[0] = order_reverse_data[4];
						a0_di[1] = order_reverse_data[5];
						a1_di[0] = 64'd0;//trivial
						a1_di[1] = 64'd0;//trivial
						end
					5 :
						begin
						a0_di[0] = order_reverse_data[4];
						a0_di[1] = order_reverse_data[5];
						a1_di[0] = 64'd0;//trivial
						a1_di[1] = 64'd0;//trivial
						end
					6 :
						begin
						a0_di[0] = order_reverse_data[6];
						a0_di[1] = order_reverse_data[7];
						a1_di[0] = 64'd0;//trivial
						a1_di[1] = 64'd0;//trivial
						end
					7 :
						begin
						a0_di[0] = order_reverse_data[6];
						a0_di[1] = order_reverse_data[7];
						a1_di[0] = 64'd0;//trivial
						a1_di[1] = 64'd0;//trivial
						end
				endcase
				end
			else
				begin
				case (cnt[2 : 0])
					0 :
						begin
						a1_di[0] = order_reverse_data[0];
						a1_di[1] = order_reverse_data[1];
						a0_di[0] = 64'd0;//trivial
						a0_di[1] = 64'd0;//trivial
						end
					1 :
						begin
						a1_di[0] = order_reverse_data[0];
						a1_di[1] = order_reverse_data[1];
						a0_di[0] = 64'd0;//trivial
						a0_di[1] = 64'd0;//trivial
						end
					2 :
						begin
						a1_di[0] = order_reverse_data[2];
						a1_di[1] = order_reverse_data[3];
						a0_di[0] = 64'd0;//trivial
						a0_di[1] = 64'd0;//trivial
						end	
					3 :
						begin
						a1_di[0] = order_reverse_data[2];
						a1_di[1] = order_reverse_data[3];
						a0_di[0] = 64'd0;//trivial
						a0_di[1] = 64'd0;//trivial
						end
					4 :
						begin
						a1_di[0] = order_reverse_data[4];
						a1_di[1] = order_reverse_data[5];
						a0_di[0] = 64'd0;//trivial
						a0_di[1] = 64'd0;//trivial
						end
					5 :
						begin
						a1_di[0] = order_reverse_data[4];
						a1_di[1] = order_reverse_data[5];
						a0_di[0] = 64'd0;//trivial
						a0_di[1] = 64'd0;//trivial
						end
					6 :
						begin
						a1_di[0] = order_reverse_data[6];
						a1_di[1] = order_reverse_data[7];
						a0_di[0] = 64'd0;//trivial
						a0_di[1] = 64'd0;//trivial
						end
					7 :
						begin
						a1_di[0] = order_reverse_data[6];
						a1_di[1] = order_reverse_data[7];
						a0_di[0] = 64'd0;//trivial
						a0_di[1] = 64'd0;//trivial
						end
				endcase
				end
			end
		FFT_A : 
			begin
			a0_di[0] = {BFU_out[0], BFU_out[2], BFU_out[4], BFU_out[6]};
			a0_di[1] = {BFU_out[1], BFU_out[3], BFU_out[5], BFU_out[7]};
			a1_di[0] = {BFU_out[0], BFU_out[2], BFU_out[4], BFU_out[6]};
			a1_di[1] = {BFU_out[1], BFU_out[3], BFU_out[5], BFU_out[7]};
			end
		default :
			begin
			a0_di[0] = 64'd0;
			a0_di[1] = 64'd0;
			a1_di[0] = 64'd0;
			a1_di[1] = 64'd0;
			end
	endcase
	end

////Order Reverse Data Setup
always @ (*)
	begin
	if (Cmd == START_NTT)////NTT Order Reverse write at a0
		begin
		case (read_addr[0][7 : 6])
			2'b00 :
				begin
				order_reverse_data[0] = {a[0], 48'd0};
				order_reverse_data[1] = {a[1], 48'd0};
				order_reverse_data[2] = {a[2], 48'd0};
				order_reverse_data[3] = {a[3], 48'd0};
				order_reverse_data[4] = {a[4], 48'd0};
				order_reverse_data[5] = {a[5], 48'd0};
				order_reverse_data[6] = {a[6], 48'd0};
				order_reverse_data[7] = {a[7], 48'd0};
				end
			2'b01 :
				begin
				order_reverse_data[0] = {32'd0, a[0], 16'd0} | a0_do[0];
				order_reverse_data[1] = {32'd0, a[1], 16'd0} | a0_do[1];
				order_reverse_data[2] = {32'd0, a[2], 16'd0} | a0_do[0];
				order_reverse_data[3] = {32'd0, a[3], 16'd0} | a0_do[1];
				order_reverse_data[4] = {32'd0, a[4], 16'd0} | a0_do[0];
				order_reverse_data[5] = {32'd0, a[5], 16'd0} | a0_do[1];
				order_reverse_data[6] = {32'd0, a[6], 16'd0} | a0_do[0];
				order_reverse_data[7] = {32'd0, a[7], 16'd0} | a0_do[1];
				end
			2'b10 :
				begin
				order_reverse_data[0] = {16'd0, a[0], 32'd0} | a0_do[0];
				order_reverse_data[1] = {16'd0, a[1], 32'd0} | a0_do[1];
				order_reverse_data[2] = {16'd0, a[2], 32'd0} | a0_do[0];
				order_reverse_data[3] = {16'd0, a[3], 32'd0} | a0_do[1];
				order_reverse_data[4] = {16'd0, a[4], 32'd0} | a0_do[0];
				order_reverse_data[5] = {16'd0, a[5], 32'd0} | a0_do[1];
				order_reverse_data[6] = {16'd0, a[6], 32'd0} | a0_do[0];
				order_reverse_data[7] = {16'd0, a[7], 32'd0} | a0_do[1];
				end
			2'b11 :
				begin
				order_reverse_data[0] = {48'd0, a[0]} | a0_do[0];
				order_reverse_data[1] = {48'd0, a[1]} | a0_do[1];
				order_reverse_data[2] = {48'd0, a[2]} | a0_do[0];
				order_reverse_data[3] = {48'd0, a[3]} | a0_do[1];
				order_reverse_data[4] = {48'd0, a[4]} | a0_do[0];
				order_reverse_data[5] = {48'd0, a[5]} | a0_do[1];
				order_reverse_data[6] = {48'd0, a[6]} | a0_do[0];
				order_reverse_data[7] = {48'd0, a[7]} | a0_do[1];
				end
		endcase
		end
	else ////NTT Order Reverse write at a1
		begin
		case (read_addr[0][7 : 6])
			2'b00 :
				begin
				order_reverse_data[0] = {a[0], 48'd0};
				order_reverse_data[1] = {a[1], 48'd0};
				order_reverse_data[2] = {a[2], 48'd0};
				order_reverse_data[3] = {a[3], 48'd0};
				order_reverse_data[4] = {a[4], 48'd0};
				order_reverse_data[5] = {a[5], 48'd0};
				order_reverse_data[6] = {a[6], 48'd0};
				order_reverse_data[7] = {a[7], 48'd0};
				end
			2'b01 :
				begin
				order_reverse_data[0] = {32'd0, a[0], 16'd0} | a1_do[0];
				order_reverse_data[1] = {32'd0, a[1], 16'd0} | a1_do[1];
				order_reverse_data[2] = {32'd0, a[2], 16'd0} | a1_do[0];
				order_reverse_data[3] = {32'd0, a[3], 16'd0} | a1_do[1];
				order_reverse_data[4] = {32'd0, a[4], 16'd0} | a1_do[0];
				order_reverse_data[5] = {32'd0, a[5], 16'd0} | a1_do[1];
				order_reverse_data[6] = {32'd0, a[6], 16'd0} | a1_do[0];
				order_reverse_data[7] = {32'd0, a[7], 16'd0} | a1_do[1];
				end
			2'b10 :
				begin
				order_reverse_data[0] = {16'd0, a[0], 32'd0} | a1_do[0];
				order_reverse_data[1] = {16'd0, a[1], 32'd0} | a1_do[1];
				order_reverse_data[2] = {16'd0, a[2], 32'd0} | a1_do[0];
				order_reverse_data[3] = {16'd0, a[3], 32'd0} | a1_do[1];
				order_reverse_data[4] = {16'd0, a[4], 32'd0} | a1_do[0];
				order_reverse_data[5] = {16'd0, a[5], 32'd0} | a1_do[1];
				order_reverse_data[6] = {16'd0, a[6], 32'd0} | a1_do[0];
				order_reverse_data[7] = {16'd0, a[7], 32'd0} | a1_do[1];
				end
			2'b11 :
				begin
				order_reverse_data[0] = {48'd0, a[0]} | a1_do[0];
				order_reverse_data[1] = {48'd0, a[1]} | a1_do[1];
				order_reverse_data[2] = {48'd0, a[2]} | a1_do[0];
				order_reverse_data[3] = {48'd0, a[3]} | a1_do[1];
				order_reverse_data[4] = {48'd0, a[4]} | a1_do[0];
				order_reverse_data[5] = {48'd0, a[5]} | a1_do[1];
				order_reverse_data[6] = {48'd0, a[6]} | a1_do[0];
				order_reverse_data[7] = {48'd0, a[7]} | a1_do[1];
				end
		endcase
		end
	end


////FFT stage W data address setting
always @(*)
	begin
	case (fft_stage)
		0 :
			begin
			w_addr[0] = 8'd0;//// First stage is always 1
			w_addr[1] = 8'd0;////
			end
		1 : 
			begin
			w_addr[0] = {read_addr[0][7], 7'd0};
			w_addr[1] = {read_addr[1][7], 7'd0};
			end
		2 : 
			begin
			w_addr[0] = {read_addr[0][7 : 6], 6'd0};
			w_addr[1] = {read_addr[1][7 : 6], 6'd0};
			end
		3 :
			begin
			w_addr[0] = {read_addr[0][7 : 5], 5'd0};
			w_addr[1] = {read_addr[1][7 : 5], 5'd0};
			end
		4 :
			begin
			w_addr[0] = {read_addr[0][7 : 4], 4'd0};
			w_addr[1] = {read_addr[1][7 : 4], 4'd0};
			end
		5 :
			begin
			w_addr[0] = {read_addr[0][7 : 3], 3'd0};
			w_addr[1] = {read_addr[1][7 : 3], 3'd0};
			end
		6 :
			begin
			w_addr[0] = {read_addr[0][7 : 2], 2'd0};
			w_addr[1] = {read_addr[1][7 : 2], 2'd0};
			end
		7 :
			begin
			w_addr[0] = {read_addr[0][7 : 1], 1'd0};
			w_addr[1] = {read_addr[1][7 : 1], 1'd0};
			end
		8 :
			begin
			w_addr[0] = {read_addr[0][7 : 0]};
			w_addr[1] = {read_addr[1][7 : 0]};
			end
		9 :
			begin
			w_addr[0] = {read_addr[0][7 : 0]};
			w_addr[1] = {read_addr[1][7 : 0]};
			end
		default :
			begin
			w_addr[0] = 0;//trivial
			w_addr[1] = 0;//trivial
			end
	endcase
	end

////General Control Address Traverse
always @ (posedge Clk)
	begin
	if (Resetn == 1'b0)
		read_ram <= 0;////NTT start from 0 ; phi 0 -> 1 ; order_rev 1 -> 0; fft 0 -> 1 -> 0 -> ... -> 0 
					  ////INTT start from 0; order_rev 0 -> 1; fft 1 -> 0 -> 1 -> ... -> 1 ; phi 1 -> 0
	else if (PHI_Fin || ORD_Fin || (stage == FFT_A && fft_stage_done == 1'b1))
		read_ram <= ~read_ram;
	else
		read_ram <= read_ram;
	end

////Address Traverse
always @ (posedge Clk)
	if (Resetn == 1'b0)
		begin
		read_addr[0] <= 0;
		read_addr[1] <= 1;
		end
	else
		begin
		read_addr[0] <= read_addr_next[0];
		read_addr[1] <= read_addr_next[1];
		end

always @ (*)
	if (PHI_Fin || ORD_Fin || FFT_Fin || (stage == FFT_A && fft_stage_done == 1'b1))
		begin
		read_addr_next[0] = 0;
		read_addr_next[1] = 1;
		end
	else if ((stage == PHI_A)|| (stage == ORDER_REV && cnt[2:0] == 7) || (stage == FFT_A)) 
		begin
		read_addr_next[0] = read_addr[0] + 2;
		read_addr_next[1] = read_addr[1] + 2;
		end
	else
		begin
		read_addr_next[0] = read_addr[0];
		read_addr_next[1] = read_addr[1];
		end

always @ (posedge Clk)
	if (Resetn == 1'b0)
		begin
		write_addr[0] <= 0;
		write_addr[1] <= 1;
		end
	else
		begin
		write_addr[0] <= write_addr_next[0];
		write_addr[1] <= write_addr_next[1];
		end

always @ (*)
	if (PHI_Fin || ORD_Fin || FFT_Fin || (stage == FFT_A && fft_stage_done == 1'b1))
		begin
		write_addr_next[0] = 0;
		write_addr_next[1] = 1;
		end
	else if (a0_we || a1_we) //traverse to next two value after write
		begin
		write_addr_next[0] = write_addr[0] + 2;
		write_addr_next[1] = write_addr[1] + 2;
		end
	else
		begin
		write_addr_next[0] = write_addr[0];
		write_addr_next[1] = write_addr[1];
		end
		
////Bram Write control
always @ (*)
	begin
	if (stage == INIT && Cmd == WRITE)
		begin
		a0_we = 1;
		a1_we = 0;
		end
	else if (stage == PHI_A && Mod_0_Complete == 1'b1)
		begin
		a0_we = (write_ram == 1'b0) ? 1 : 0;
		a1_we = (write_ram == 1'b0) ? 0 : 1;
		end
	else if (stage == ORDER_REV)
		begin
		if (cnt[2:0] == 1 || cnt[2:0] == 3 || cnt[2:0] == 5 || cnt[2:0] == 7)
			begin
			a0_we = (write_ram == 1'b0) ? 1 : 0; 
			a1_we = (write_ram == 1'b0) ? 0 : 1;
			end
		else 
			begin
			a0_we = 0; 
			a1_we = 0;
			end
		end
	else if (stage == FFT_A && BFU_done == 1'b1)
		begin
		a0_we = (write_ram == 1'b0) ? 1 : 0;
		a1_we = (write_ram == 1'b0) ? 0 : 1;
		end
	else
		begin
		a0_we = 0;
		a1_we = 0;
		end
	end

////General Usage Counter
always @ (posedge Clk)
	begin
	if (Resetn == 1'b0 || PHI_Fin || ORD_Fin || (stage == FFT_A && fft_stage_done == 1'b1))
		cnt <= 0;
	else if (stage == PHI_A || stage == ORDER_REV || stage == FFT_A)
		cnt <= cnt + 1'b1;
	else 
		cnt <= cnt;
	end

////Phi stage mod 0 data feed 
always @ (*)
	begin
	if (stage == PHI_A )
		begin
		for (i = 0; i < N; i = i + 1)
			Mod_0_in[i] = a[i] * phi[i]; 	
		end
	else
		begin
		for (i = 0; i < N; i = i + 1)
			Mod_0_in[i] = 0; 	
		end
	end

////FFT stage control counter	
always @ (posedge Clk)
	if (Resetn == 1'b0 || FFT_Fin)
		fft_stage <= 0;
	else if (stage == FFT_A && fft_stage_done == 1)
		fft_stage <= fft_stage + 1'b1;
	else
		fft_stage <= fft_stage;

endmodule
