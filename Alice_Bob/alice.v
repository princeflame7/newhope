`timescale 1ns / 1ps

module alice (
	Clk,
	Reset,
	Cmd,
	Addr,
	Data_In,
	Data_Out,
	Valid
);

parameter	P_WIDTH		= 16;
parameter 	N 			= 8;

////Cmd
parameter	START		= 1;
parameter 	U_INPUT 	= 2;
parameter 	WRITE_U 	= 3;
parameter	WRITE_R		= 5;
parameter	START_P2 	= 4;
parameter	READ_SEED	= 6;
parameter	READ_SS		= 7;

////State
parameter	IDLE		= 0;
parameter	GEN_S		= 1;////Generate S write into NTT
parameter	NTT_S		= 2;
parameter	SAVE_S		= 3;////Save S from NTT to BRAM
parameter	GEN_E		= 4;////Generate E write into NTT
parameter	NTT_E		= 5;
parameter	COM_B		= 6;////Generate A and Compute b = a.s+e
parameter 	WAIT_U 		= 7;
parameter 	INPUT_U 	= 8;
parameter 	COM_US		= 9;////Compute u.s
parameter 	INTT_V 		= 10;////Compute V
parameter	RECON		= 11;////Reconciliation

input 								Clk;
input								Reset;
input 			[2 	: 0] 			Cmd;
input			[7 : 0] 			Addr;
input			[63 : 0]			Data_In;
output			[63 : 0]			Data_Out;
output reg							Valid; 

reg				[2047 : 0]			R;
reg				[255 : 0]			Seed;
reg				[255 : 0]			Share_Secret;
reg 			[4 : 0]				state;
reg				[4 : 0]				state_next;
integer 							i;

////Module Port
wire								SHAKE_Reset;
wire			[255 : 0]			TRN;
reg				[255 : 0]			TRN_buffer;

reg				[1343 : 0]			true_random_seed;
reg									true_random_seed_ready;
wire								ack;
wire			[63 : 0]			sampling_in;
wire								sampling_in_ready;

wire			[15 : 0]			sampling_out_0;
wire			[15 : 0]			sampling_out_1;
wire			[15 : 0]			sampling_out_2;
wire			[15 : 0]			sampling_out_3;
wire								sampling_out_ready_0;
wire								sampling_out_ready_1;
wire								sampling_out_ready_2;
wire								sampling_out_ready_3;
wire			[2:0]				sampling_out_ready_sum;

wire			[13 : 0]			sampling_out_bi_0;
wire			[13 : 0]			sampling_out_bi_1;
wire			[13 : 0]			sampling_out_bi_2;
wire			[13 : 0]			sampling_out_bi_3;
wire								sampling_out_ready_bi;

reg 								s_we;
wire 			[7 : 0]				s_addr 	[0 : 1];
wire 			[P_WIDTH*4 -1 : 0]	s_di 	[0 : 1];
wire 			[P_WIDTH*4 -1 : 0]	s_do 	[0 : 1];

wire 								u_we;
wire 			[7 : 0]				u_addr 	[0 : 1];
wire 			[P_WIDTH*4 -1 : 0]	u_di 	[0 : 1];
wire 			[P_WIDTH*4 -1 : 0]	u_do 	[0 : 1];

reg				[2 : 0]				NTT_Cmd;
reg				[15 : 0]			NTT_Addr;
reg				[P_WIDTH*8 -1 : 0]	NTT_DI;
wire			[P_WIDTH*8 -1 : 0]	NTT_DO;
reg				[P_WIDTH*8 -1 : 0]	NTT_DO_MOD;
wire								NTT_Valid;

reg				[31 : 0]			Mod_In			[7 : 0];
reg				[31 : 0]			mod_in_next		[7 : 0];
wire			[15 : 0]			Mod_out 		[7 : 0];

reg				[31 : 0]			V_MOD_0_0;
reg				[31 : 0]			V_MOD_0_1;
reg				[31 : 0]			V_MOD_0_2;
reg				[31 : 0]			V_MOD_0_3;
reg				[31 : 0]			V_MOD_1_0;
reg				[31 : 0]			V_MOD_1_1;
reg				[31 : 0]			V_MOD_1_2;
reg				[31 : 0]			V_MOD_1_3;
reg				[31:0]				REC_0_W0;
reg				[31:0]				REC_0_W1;
reg				[31:0]				REC_0_W2;
reg				[31:0]				REC_0_W3;
reg				[31:0]				REC_1_W0;
reg				[31:0]				REC_1_W1;
reg				[31:0]				REC_1_W2;
reg				[31:0]				REC_1_W3;
reg				[1:0]				REC_0_R0;
reg				[1:0]				REC_0_R1;
reg				[1:0]				REC_0_R2;
reg				[1:0]				REC_0_R3;
reg				[1:0]				REC_1_R0;
reg				[1:0]				REC_1_R1;
reg				[1:0]				REC_1_R2;
reg				[1:0]				REC_1_R3;
wire								REC_0_SECRET;
wire								REC_1_SECRET;

////State Reg Wire Declaration
reg 			[15 : 0]			cnt;

reg 			[4 : 0]				cnt_coef;

wire 								GEN_S_Fin;
wire 								NTT_S_Fin;
wire 								SAVE_S_Fin;
wire 								GEN_E_Fin;
wire 								NTT_E_Fin;
wire 								COM_B_Fin;
wire 								COM_US_Fin;
wire 								INTT_V_Fin;
wire								RECON_Fin;

reg				[P_WIDTH*8 -1 : 0]	Gen_Coef;
reg				[P_WIDTH*8 -1 : 0]  Gen_Coef_Buff;
reg				[7 : 0]				Gen_Coef_Buff_Flag;
wire			[15 : 0]			Gen_Coef_Next_0;
wire			[15 : 0]			Gen_Coef_Next_1;
wire			[15 : 0]			Gen_Coef_Next_2;
wire			[15 : 0]			Gen_Coef_Next_3;
wire			[15 : 0]			Gen_Coef_Next_4;
wire			[15 : 0]			Gen_Coef_Next_5;
wire			[15 : 0]			Gen_Coef_Next_6;
wire			[15 : 0]			Gen_Coef_Next_7;
wire			[15 : 0]			Gen_Coef_Buff_Next_5;
wire			[15 : 0]			Gen_Coef_Buff_Next_6;
wire			[15 : 0]			Gen_Coef_Buff_Next_7;
wire								Gen_Coef_Buff_Flag_Next_5;
wire								Gen_Coef_Buff_Flag_Next_6;
wire								Gen_Coef_Buff_Flag_Next_7;
wire 			[15 : 0]			coef_0;
wire 			[15 : 0]			coef_1;
wire 			[15 : 0]			coef_2;
wire 			[15 : 0]			coef_3;
wire			[3 : 0]				coef_0_index;
wire			[3 : 0]				coef_1_index;
wire			[3 : 0]				coef_2_index;
wire			[3 : 0]				coef_3_index;
wire 								write_s;
reg 								Kred_Done_0;
reg 								Kred_Done_1;
reg 								Kred_Done_2;
reg 								Kred_Done_3;

reg				[8 : 0]				read_addr 		[0 : 1];
reg				[8 : 0]				read_addr_next 	[0 : 1];
reg				[8 : 0]				write_addr 		[0 : 1];
reg				[8 : 0]				write_addr_next	[0 : 1];

////Module Decalaration
TRNG TRNG (.clk(Clk), .TRN(TRN));
//Module directly ask sampler, sampler interacts with shake to construct results
shake128output64 SHAKE (
	.clk(Clk),
	.reset(SHAKE_Reset),
	.in(true_random_seed), 
	.in_ready(true_random_seed_ready),
	.next(1'b1),
	
	.ack(),
	.out(sampling_in),
	.out_ready(sampling_in_ready)
);

sampling12289 SAMPLE_0 (
	.clk(Clk),
	.reset(SHAKE_Reset), 
	.in(sampling_in[15:0]),
	.in_ready(sampling_in_ready),
	.next(1'b1),
	
	.need(),
	.out(sampling_out_0),
	.out_ready(sampling_out_ready_0)
);

sampling12289 SAMPLE_1 (
	.clk(Clk),
	.reset(SHAKE_Reset), 
	.in(sampling_in[31:16]),
	.in_ready(sampling_in_ready),
	.next(1'b1),
	
	.need(),
	.out(sampling_out_1),
	.out_ready(sampling_out_ready_1)
);

sampling12289 SAMPLE_2 (
	.clk(Clk),
	.reset(SHAKE_Reset), 
	.in(sampling_in[47:32]),
	.in_ready(sampling_in_ready),
	.next(1'b1),
	
	.need(),
	.out(sampling_out_2),
	.out_ready(sampling_out_ready_2)
);

sampling12289 SAMPLE_3 (
	.clk(Clk),
	.reset(SHAKE_Reset), 
	.in(sampling_in[63:48]),
	.in_ready(sampling_in_ready),
	.next(1'b1),
	
	.need(),
	.out(sampling_out_3),
	.out_ready(sampling_out_ready_3)
);

sampling12289_binomial16 SAMPLE_bi_0 (
	.clk(Clk),
	.reset(SHAKE_Reset), 
	.in(sampling_in[15:0]),
	.in_ready(sampling_in_ready),
	.next(1'b1),
	
	.need(),
	.out(sampling_out_bi_0),
	.out_ready(sampling_out_ready_bi)
);

sampling12289_binomial16 SAMPLE_bi_1 (
	.clk(Clk),
	.reset(SHAKE_Reset), 
	.in(sampling_in[31:16]),
	.in_ready(sampling_in_ready),
	.next(1'b1),
	
	.need(),
	.out(sampling_out_bi_1),
	.out_ready()
);

sampling12289_binomial16 SAMPLE_bi_2 (
	.clk(Clk),
	.reset(SHAKE_Reset), 
	.in(sampling_in[47:32]),
	.in_ready(sampling_in_ready),
	.next(1'b1),
	
	.need(),
	.out(sampling_out_bi_2),
	.out_ready()
);

sampling12289_binomial16 SAMPLE_bi_3 (
	.clk(Clk),
	.reset(SHAKE_Reset), 
	.in(sampling_in[63:48]),
	.in_ready(sampling_in_ready),
	.next(1'b1),
	
	.need(),
	.out(sampling_out_bi_3),
	.out_ready()
);

bram_dual_kred s (.Clk(Clk), .En(1'b1), .We(s_we), .Addr_A(s_addr[0]), .Addr_B(s_addr[1]), .DI_A(s_di[0]), .DI_B(s_di[1]), .DO_A(s_do[0]), .DO_B(s_do[1]));

bram_dual_kred u (.Clk(Clk), .En(1'b1), .We(u_we), .Addr_A(u_addr[0]), .Addr_B(u_addr[1]), .DI_A(u_di[0]), .DI_B(u_di[1]), .DO_A(u_do[0]), .DO_B(u_do[1]));

NTT_1024_Mod12289_kred NTT (
	.Clk(Clk), .Resetn(~Reset),
	.Cmd(NTT_Cmd), .Addr(NTT_Addr),
	.DI(NTT_DI), .DO(NTT_DO),
	.Valid(NTT_Valid)
);

Rec REC_0 (
	.clk(Clk),
	.w0(REC_0_W0[29:16]),
	.w1(REC_0_W1[29:16]),
	.w2(REC_0_W2[29:16]),
	.w3(REC_0_W3[29:16]),
	.r0(REC_0_R0),
	.r1(REC_0_R1),
	.r2(REC_0_R2),
	.r3(REC_0_R3),

	.secret(REC_0_SECRET)
);

Rec REC_1 (
	.clk(Clk),
	.w0(REC_1_W0[29:16]),
	.w1(REC_1_W1[29:16]),
	.w2(REC_1_W2[29:16]),
	.w3(REC_1_W3[29:16]),
	.r0(REC_1_R0),
	.r1(REC_1_R1),
	.r2(REC_1_R2),
	.r3(REC_1_R3),

	.secret(REC_1_SECRET)
);

kredUV Mod_0 (.clk(Clk), .resetn(~Reset),  .inU(16'd0), .inV(Mod_In[0]), .outUpV(Mod_out[0]), .outUmV());
kredUV Mod_1 (.clk(Clk), .resetn(~Reset),  .inU(16'd0), .inV(Mod_In[1]), .outUpV(Mod_out[1]), .outUmV());
kredUV Mod_2 (.clk(Clk), .resetn(~Reset),  .inU(16'd0), .inV(Mod_In[2]), .outUpV(Mod_out[2]), .outUmV());
kredUV Mod_3 (.clk(Clk), .resetn(~Reset),  .inU(16'd0), .inV(Mod_In[3]), .outUpV(Mod_out[3]), .outUmV());
kredUV Mod_4 (.clk(Clk), .resetn(~Reset),  .inU(16'd0), .inV(Mod_In[4]), .outUpV(Mod_out[4]), .outUmV());
kredUV Mod_5 (.clk(Clk), .resetn(~Reset),  .inU(16'd0), .inV(Mod_In[5]), .outUpV(Mod_out[5]), .outUmV());
kredUV Mod_6 (.clk(Clk), .resetn(~Reset),  .inU(16'd0), .inV(Mod_In[6]), .outUpV(Mod_out[6]), .outUmV());
kredUV Mod_7 (.clk(Clk), .resetn(~Reset),  .inU(16'd0), .inV(Mod_In[7]), .outUpV(Mod_out[7]), .outUmV());

assign SHAKE_Reset = Reset || ((state == GEN_S) && (cnt == 0)) || ((state == GEN_E) && (cnt == 0)) || ((state == COM_B) && (cnt == 0));
assign Data_Out =
	(Cmd == READ_SEED) ? Seed[64*Addr[1:0]+:64] :
	(Cmd == READ_SS) ? Share_Secret[64*Addr[1:0]+:64] :
	(state == WAIT_U) ? u_do[1] :
	0;

assign GEN_S_Fin = (state == GEN_S) && (write_addr[0] == 254) && (NTT_Cmd == 1);
assign NTT_S_Fin = (state == NTT_S) && NTT_Valid;
assign SAVE_S_Fin = (state == SAVE_S) && (write_addr[0] == 254);
assign GEN_E_Fin = (state == GEN_E) && (write_addr[0] == 254) && (NTT_Cmd == 1);
assign NTT_E_Fin = (state == NTT_E) && NTT_Valid;
assign COM_B_Fin = (state == COM_B) && (write_addr[0] == 254 && u_we == 1);
assign COM_US_Fin = (state == COM_US) && (write_addr[0] == 254);
assign INTT_V_Fin = (state == INTT_V) && NTT_Valid;
assign RECON_Fin = (state == RECON) && (cnt == 134);

assign coef_0 = (state == COM_B) ? sampling_out_0 : sampling_out_bi_0;
assign coef_1 = (state == COM_B) ? sampling_out_1 : sampling_out_bi_1;
assign coef_2 = (state == COM_B) ? sampling_out_2 : sampling_out_bi_2;
assign coef_3 = (state == COM_B) ? sampling_out_3 : sampling_out_bi_3;
assign coef_3_index = (cnt_coef >= 8) ? cnt_coef - 8 : cnt_coef;
assign coef_2_index = sampling_out_ready_3 ? coef_3_index+1 : coef_3_index;
assign coef_1_index = sampling_out_ready_2 ? coef_2_index+1 : coef_2_index;
assign coef_0_index = sampling_out_ready_1 ? coef_1_index+1 : coef_1_index;
assign sampling_out_ready_sum = sampling_out_ready_3 + sampling_out_ready_2 + sampling_out_ready_1 + sampling_out_ready_0;

assign Gen_Coef_Next_7 =
	(sampling_out_ready_3 == 1 && coef_3_index == 0) ? coef_3 :
	(sampling_out_ready_2 == 1 && coef_2_index == 0) ? coef_2 :
	(sampling_out_ready_1 == 1 && coef_1_index == 0) ? coef_1 :
	(sampling_out_ready_0 == 1 && coef_0_index == 0) ? coef_0 :
	(Gen_Coef_Buff_Flag[7] == 1) ? Gen_Coef_Buff[127:112] : Gen_Coef[127:112];
assign Gen_Coef_Next_6 =
	(sampling_out_ready_3 == 1 && coef_3_index == 1) ? coef_3 :
	(sampling_out_ready_2 == 1 && coef_2_index == 1) ? coef_2 :
	(sampling_out_ready_1 == 1 && coef_1_index == 1) ? coef_1 :
	(sampling_out_ready_0 == 1 && coef_0_index == 1) ? coef_0 :
	(Gen_Coef_Buff_Flag[6] == 1) ? Gen_Coef_Buff[111:96] : Gen_Coef[111:96];
assign Gen_Coef_Next_5 =
	(sampling_out_ready_3 == 1 && coef_3_index == 2) ? coef_3 :
	(sampling_out_ready_2 == 1 && coef_2_index == 2) ? coef_2 :
	(sampling_out_ready_1 == 1 && coef_1_index == 2) ? coef_1 :
	(sampling_out_ready_0 == 1 && coef_0_index == 2) ? coef_0 :
	(Gen_Coef_Buff_Flag[5] == 1) ? Gen_Coef_Buff[95:80] : Gen_Coef[95:80];
assign Gen_Coef_Next_4 =
	(sampling_out_ready_3 == 1 && coef_3_index == 3) ? coef_3 :
	(sampling_out_ready_2 == 1 && coef_2_index == 3) ? coef_2 :
	(sampling_out_ready_1 == 1 && coef_1_index == 3) ? coef_1 :
	(sampling_out_ready_0 == 1 && coef_0_index == 3) ? coef_0 : Gen_Coef[79:64];
assign Gen_Coef_Next_3 =
	(sampling_out_ready_3 == 1 && coef_3_index == 4) ? coef_3 :
	(sampling_out_ready_2 == 1 && coef_2_index == 4) ? coef_2 :
	(sampling_out_ready_1 == 1 && coef_1_index == 4) ? coef_1 :
	(sampling_out_ready_0 == 1 && coef_0_index == 4) ? coef_0 : Gen_Coef[63:48];
assign Gen_Coef_Next_2 =
	(sampling_out_ready_3 == 1 && coef_3_index == 5) ? coef_3 :
	(sampling_out_ready_2 == 1 && coef_2_index == 5) ? coef_2 :
	(sampling_out_ready_1 == 1 && coef_1_index == 5) ? coef_1 :
	(sampling_out_ready_0 == 1 && coef_0_index == 5) ? coef_0 : Gen_Coef[47:32];
assign Gen_Coef_Next_1 =
	(sampling_out_ready_3 == 1 && coef_3_index == 6) ? coef_3 :
	(sampling_out_ready_2 == 1 && coef_2_index == 6) ? coef_2 :
	(sampling_out_ready_1 == 1 && coef_1_index == 6) ? coef_1 :
	(sampling_out_ready_0 == 1 && coef_0_index == 6) ? coef_0 : Gen_Coef[31:16];
assign Gen_Coef_Next_0 =
	(sampling_out_ready_3 == 1 && coef_3_index == 7) ? coef_3 :
	(sampling_out_ready_2 == 1 && coef_2_index == 7) ? coef_2 :
	(sampling_out_ready_1 == 1 && coef_1_index == 7) ? coef_1 :
	(sampling_out_ready_0 == 1 && coef_0_index == 7) ? coef_0 : Gen_Coef[15:0];

assign Gen_Coef_Buff_Next_7 =
	(sampling_out_ready_3 == 1 && coef_3_index == 8) ? coef_3 :
	(sampling_out_ready_2 == 1 && coef_2_index == 8) ? coef_2 :
	(sampling_out_ready_1 == 1 && coef_1_index == 8) ? coef_1 :
	(sampling_out_ready_0 == 1 && coef_0_index == 8) ? coef_0 : 0;
assign Gen_Coef_Buff_Next_6 =
	(sampling_out_ready_3 == 1 && coef_3_index == 9) ? coef_3 :
	(sampling_out_ready_2 == 1 && coef_2_index == 9) ? coef_2 :
	(sampling_out_ready_1 == 1 && coef_1_index == 9) ? coef_1 :
	(sampling_out_ready_0 == 1 && coef_0_index == 9) ? coef_0 : 0;
assign Gen_Coef_Buff_Next_5 =
	(sampling_out_ready_3 == 1 && coef_3_index == 10) ? coef_3 :
	(sampling_out_ready_2 == 1 && coef_2_index == 10) ? coef_2 :
	(sampling_out_ready_1 == 1 && coef_1_index == 10) ? coef_1 :
	(sampling_out_ready_0 == 1 && coef_0_index == 10) ? coef_0 : 0;

assign Gen_Coef_Buff_Flag_Next_7 =
	(sampling_out_ready_3 == 1 && coef_3_index == 8) ? 1 :
	(sampling_out_ready_2 == 1 && coef_2_index == 8) ? 1 :
	(sampling_out_ready_1 == 1 && coef_1_index == 8) ? 1 :
	(sampling_out_ready_0 == 1 && coef_0_index == 8) ? 1 : 0;
assign Gen_Coef_Buff_Flag_Next_6 =
	(sampling_out_ready_3 == 1 && coef_3_index == 9) ? 1 :
	(sampling_out_ready_2 == 1 && coef_2_index == 9) ? 1 :
	(sampling_out_ready_1 == 1 && coef_1_index == 9) ? 1 :
	(sampling_out_ready_0 == 1 && coef_0_index == 9) ? 1 : 0;
assign Gen_Coef_Buff_Flag_Next_5 =
	(sampling_out_ready_3 == 1 && coef_3_index == 10) ? 1 :
	(sampling_out_ready_2 == 1 && coef_2_index == 10) ? 1 :
	(sampling_out_ready_1 == 1 && coef_1_index == 10) ? 1 :
	(sampling_out_ready_0 == 1 && coef_0_index == 10) ? 1 : 0;


assign write_s = (state == SAVE_S) && (cnt >= 1 && cnt <= 128);//enable at clk 2 of save_S, clk0: reseting ntt, clk1: read 0 1, clk2: data 0 1 retrieved, write 

////FSM
always @ (posedge Clk)
	begin
	if (Reset)
		state <= IDLE;
	else
		state <= state_next;
	end

always @ (*)
	case (state)
		IDLE :
			begin
			if (Cmd == START)
				state_next = GEN_S;
			else
				state_next = IDLE;
			end
		GEN_S :
			begin
			if (GEN_S_Fin)
				state_next = NTT_S;
			else
				state_next = GEN_S;
			end
		NTT_S :
			begin
			if (NTT_S_Fin)
				state_next = SAVE_S;
			else
				state_next = NTT_S;
			end
		SAVE_S :
			begin
			if (SAVE_S_Fin)
				state_next = GEN_E;
			else
				state_next = SAVE_S;
			end
		GEN_E :
			begin
			if (GEN_E_Fin)
				state_next = NTT_E;
			else
				state_next = GEN_E;
			end
		NTT_E :
			begin
			if (NTT_E_Fin)
				state_next = COM_B;
			else
				state_next = NTT_E;
			end
		COM_B :
			begin
			if (COM_B_Fin)
				state_next = WAIT_U;
			else
				state_next = COM_B;
			end
		WAIT_U :
			begin
			if (Cmd == U_INPUT)
				state_next = INPUT_U;
			else 
				state_next = WAIT_U;
			end
		INPUT_U : 
			begin 
			if (Cmd == START_P2)
				state_next = COM_US;
			else
				state_next = INPUT_U;
			end
		COM_US :
			begin
			if (COM_US_Fin)
				state_next = INTT_V;
			else 
				state_next = COM_US;
			end
		INTT_V :
			begin
			if (INTT_V_Fin)
				state_next = RECON;
			else
				state_next = INTT_V;
			end
		RECON :
			begin
			if (RECON_Fin)
				state_next = IDLE;
			else
				state_next = RECON;
			end
		default :
			state_next = state;
	endcase

always @ (posedge Clk) begin
	if (Reset) begin
		R <= 0;
	end
	else if (state == INPUT_U && Cmd == WRITE_R) begin
		R[64*Addr[4:0]+:64] <= Data_In;
	end
end

///Output
always @ (posedge Clk)
	if (Reset)
		begin
		Valid <= 0;
		Seed <= 0;
		Share_Secret <= 0;
		end
	else if (COM_B_Fin)
		begin
		Valid <= 1;
		Seed <= TRN_buffer;
		Share_Secret <= 0;
		end
	else if (Cmd == U_INPUT)
		begin
		Valid <= 0;
		Seed <= Seed;
		Share_Secret <= 0;
		end
	else if (state == RECON && cnt >= 7 && cnt <= 134)
		begin
		Share_Secret[2*(cnt-7)+:2] <= {REC_1_SECRET, REC_0_SECRET};
		if (RECON_Fin) begin
			Valid <= 1;
		end
		end
	else
		begin
		Valid <= Valid;
		Seed <= Seed;
		Share_Secret <= Share_Secret;
		end

////counter
always @ (posedge Clk)
	if (Reset || GEN_S_Fin || NTT_S_Fin || SAVE_S_Fin || GEN_E_Fin || NTT_E_Fin || COM_B_Fin || COM_US_Fin || RECON_Fin)
		cnt <= 0;////Reset at state changing
	else if (state == GEN_S || state == SAVE_S || state == GEN_E || state == COM_B || state == COM_US || state == RECON)
		cnt <= cnt + 1'b1;
	else 
		cnt <= cnt;

////Sampling Activation
always @ (posedge Clk)
	if (Reset)
		begin
		TRN_buffer <= 0;
		true_random_seed <= 0;
		true_random_seed_ready <= 0;
		end
	else if (cnt == 0 && (state == GEN_S || state == GEN_E || state == COM_B))///Trigger sampling
		begin
		TRN_buffer <= TRN;
		true_random_seed <= {TRN, 64'h1F, 960'h0, 64'h8};
		true_random_seed_ready <= 1;
		end
	else if (GEN_S_Fin || GEN_E_Fin || COM_B_Fin)///Stage End shut down shake
		begin
		TRN_buffer <= TRN_buffer;
		true_random_seed <= true_random_seed;
		true_random_seed_ready <= 0;		
		end
	else 
		begin
		TRN_buffer <= TRN_buffer;
		true_random_seed <= true_random_seed;
		true_random_seed_ready <= 0;
		end

////Coefficient Generation 
always @ (posedge Clk)///count how many coefficient has been generated
	if (Reset || GEN_S_Fin || GEN_E_Fin) begin
		cnt_coef <= 0;
	end
	else if (state == GEN_S || state == GEN_E) begin
		if (cnt_coef == 8) begin
			cnt_coef <= 0;
		end
		else if (sampling_in_ready && sampling_out_ready_bi) begin
			cnt_coef <= cnt_coef + 4;
		end
	end
	else if (state == COM_B && cnt > 0) begin
		if (cnt_coef >= 8) begin
			cnt_coef <= cnt_coef + sampling_out_ready_sum - 8;
		end
		else if (sampling_in_ready) begin
			cnt_coef <= cnt_coef + sampling_out_ready_sum;
		end
	end

always @ (posedge Clk) begin
	if (Reset || GEN_S_Fin || GEN_E_Fin) begin
		Gen_Coef <= 0;
	end
	else if (state == GEN_S && sampling_out_ready_bi) begin
		Gen_Coef[16*(4-cnt_coef) +: 64] <= {coef_3, coef_2, coef_1, coef_0};
	end
	else if (state == GEN_E && sampling_out_ready_bi) begin
		Gen_Coef[16*(4-cnt_coef) +: 64] <= {coef_3, coef_2, coef_1, coef_0};
	end
	else if (state == COM_B && cnt > 0) begin
		Gen_Coef <= {Gen_Coef_Next_7, Gen_Coef_Next_6, Gen_Coef_Next_5, Gen_Coef_Next_4, Gen_Coef_Next_3, Gen_Coef_Next_2, Gen_Coef_Next_1, Gen_Coef_Next_0};
	end
end

always @ (posedge Clk) begin
	if (Reset) begin
		Gen_Coef_Buff <= 0;
		Gen_Coef_Buff_Flag <= 0;
	end
	else if (state == COM_B && cnt > 0) begin
		Gen_Coef_Buff <= {Gen_Coef_Buff_Next_7, Gen_Coef_Buff_Next_6, Gen_Coef_Buff_Next_5, 80'd0};	
		Gen_Coef_Buff_Flag <= {Gen_Coef_Buff_Flag_Next_7, Gen_Coef_Buff_Flag_Next_6, Gen_Coef_Buff_Flag_Next_5, 5'd0};
	end
end

////NTT data address assign
always @ (*)
	case (state)
		GEN_S :
			NTT_DI = Gen_Coef;
		GEN_E :
			NTT_DI = Gen_Coef;
		COM_US :
			NTT_DI = {Mod_out[7], Mod_out[6], Mod_out[5], Mod_out[4], Mod_out[3], Mod_out[2], Mod_out[1], Mod_out[0]};
		default :
			NTT_DI = 0;
	endcase

always @ (*)
	case (state)
		GEN_S :
			NTT_Addr = {write_addr[0][7 : 0], write_addr[1][7 : 0]};
		SAVE_S :
			NTT_Addr = {read_addr[0][7 : 0], read_addr[1][7 : 0]};
		GEN_E :
			NTT_Addr = {write_addr[0][7 : 0], write_addr[1][7 : 0]};
		COM_B :
			NTT_Addr = {read_addr[0][7 : 0], read_addr[1][7 : 0]};
		COM_US :
			NTT_Addr = {write_addr[0][7 : 0], write_addr[1][7 : 0]};
		RECON :
			NTT_Addr = {read_addr[0][7 : 0], read_addr[1][7 : 0]};
		default :
			NTT_Addr = 0;
	endcase

////NTT cmd 
always @ (posedge Clk)
	if (((state == GEN_S || state == GEN_E) && cnt_coef == 4'd8) || (state == COM_US && Kred_Done_2))
		NTT_Cmd <= 1;
	else if (NTT_Valid)
		NTT_Cmd <= 4; ///close NTT comand
	else if (state == NTT_S || state == NTT_E)
		NTT_Cmd <= 2;
	else if (state == INTT_V)
		NTT_Cmd <= 3;
	else 
		NTT_Cmd <= 0;


////Bram data addr
assign s_di[0] = NTT_DO[127 : 64];
assign s_di[1] = NTT_DO[ 63 :  0];
assign u_di[0] =
	(state == COM_B) ? {Mod_out[7], Mod_out[6], Mod_out[5], Mod_out[4]} :
	(state == INPUT_U) ? Data_In[63 : 0] :
	0;
assign u_di[1] =
	(state == COM_B) ? {Mod_out[3], Mod_out[2], Mod_out[1], Mod_out[0]} :
	(state == INPUT_U) ? Data_In[63 : 0] :
	0;

assign s_addr[0] =
	(state == SAVE_S) ? write_addr[0][7 : 0] :
	(state == COM_B) ? read_addr[0][7 : 0] :
	(state == COM_US) ? read_addr[0][7 : 0] :
	0;
assign s_addr[1] =
	(state == SAVE_S) ? write_addr[1][7 : 0] :
	(state == COM_B) ? read_addr[1][7 : 0] :
	(state == COM_US) ? read_addr[1][7 : 0] :
	0;
assign u_addr[0] =
	(state == COM_B) ? write_addr[0][7 : 0] :
	(state == WAIT_U) ? Addr[7 : 0] :
	(state == INPUT_U) ? Addr[7 : 0] :
	(state == COM_US) ? read_addr[0][7 : 0]
	: 0;
assign u_addr[1] =
	(state == COM_B) ? write_addr[1][7 : 0] :
	(state == WAIT_U) ? Addr[7 : 0] :
	(state == INPUT_U) ? Addr[7 : 0] :
	(state == COM_US) ? read_addr[1][7 : 0]
	: 0;

////Bram write enable
always @ (posedge Clk)
	if (Reset)
		s_we <= 0;
	else if (state == SAVE_S && write_s)
		s_we <= 1;
	else 
		s_we <= 0;
assign u_we =
	(state == COM_B && Kred_Done_3) ? 1 :
	(state == INPUT_U && Cmd == WRITE_U) ? 1 :
	0;

always @ (posedge Clk) begin
	if (Reset) begin
		for (i = 0; i < N; i = i + 1) begin
			Mod_In[i] <= 0;
		end
	end
	else begin
		for (i = 0; i < N; i = i + 1) begin
			Mod_In[i] <= mod_in_next[i];
		end
	end
end

////Mod input
always @ (*) begin
	if (state == COM_B && cnt_coef >= 8) begin
		mod_in_next[7] = Gen_Coef[127:112] * s_do[0][63:48] + NTT_DO[127:112];
		mod_in_next[6] = Gen_Coef[111:96] * s_do[0][47:32] + NTT_DO[111:96];
		mod_in_next[5] = Gen_Coef[95:80] * s_do[0][31:16] + NTT_DO[95:80];
		mod_in_next[4] = Gen_Coef[79:64] * s_do[0][15:0] + NTT_DO[79:64];
		mod_in_next[3] = Gen_Coef[63:48] * s_do[1][63:48] + NTT_DO[63:48];
		mod_in_next[2] = Gen_Coef[47:32] * s_do[1][47:32] + NTT_DO[47:32];
		mod_in_next[1] = Gen_Coef[31:16] * s_do[1][31:16] + NTT_DO[31:16];
		mod_in_next[0] = Gen_Coef[15:0] * s_do[1][15:0] + NTT_DO[15:0];
	end
	else if (state == COM_US && cnt > 0) begin
		mod_in_next[7] = u_do[0][63:48] * s_do[0][63:48];
		mod_in_next[6] = u_do[0][47:32] * s_do[0][47:32];
		mod_in_next[5] = u_do[0][31:16] * s_do[0][31:16];
		mod_in_next[4] = u_do[0][15:0] * s_do[0][15:0];
		mod_in_next[3] = u_do[1][63:48] * s_do[1][63:48];
		mod_in_next[2] = u_do[1][47:32] * s_do[1][47:32];
		mod_in_next[1] = u_do[1][31:16] * s_do[1][31:16];
		mod_in_next[0] = u_do[1][15:0] * s_do[1][15:0];
	end
	else begin
		mod_in_next[7] = 0;
		mod_in_next[6] = 0;
		mod_in_next[5] = 0;
		mod_in_next[4] = 0;
		mod_in_next[3] = 0;
		mod_in_next[2] = 0;
		mod_in_next[1] = 0;
		mod_in_next[0] = 0;
	end
end

always @ (posedge Clk) begin
	if (state == COM_B && cnt_coef >= 8) begin
		Kred_Done_0 <= 1;
	end
	else if (state == COM_US && cnt > 0 && cnt <= 128) begin
		Kred_Done_0 <= 1;
	end
	else begin
		Kred_Done_0 <= 0;
	end
	Kred_Done_1 <= Kred_Done_0;
	Kred_Done_2 <= Kred_Done_1;
	Kred_Done_3 <= Kred_Done_2;
end

always @ (*) begin
	for (i=0; i<8; i=i+1) begin
		if (NTT_DO[i*16+:16] < 12289 * 1) begin
			NTT_DO_MOD[i*16+:16] = NTT_DO[i*16+:16] - 12289 * 0;
		end
		else if (NTT_DO[i*16+:16] < 12289 * 2) begin
			NTT_DO_MOD[i*16+:16] = NTT_DO[i*16+:16] - 12289 * 1;
		end
		else if (NTT_DO[i*16+:16] < 12289 * 3) begin
			NTT_DO_MOD[i*16+:16] = NTT_DO[i*16+:16] - 12289 * 2;
		end
		else if (NTT_DO[i*16+:16] < 12289 * 4) begin
			NTT_DO_MOD[i*16+:16] = NTT_DO[i*16+:16] - 12289 * 3;
		end
		else if (NTT_DO[i*16+:16] < 12289 * 5) begin
			NTT_DO_MOD[i*16+:16] = NTT_DO[i*16+:16] - 12289 * 4;
		end
		else begin
			NTT_DO_MOD[i*16+:16] = 0;
		end
	end
end

always @ (posedge Clk) begin
	if (Reset) begin
		V_MOD_0_0 <= 0;
		V_MOD_0_1 <= 0;
		V_MOD_0_2 <= 0;
		V_MOD_0_3 <= 0;
		V_MOD_1_0 <= 0;
		V_MOD_1_1 <= 0;
		V_MOD_1_2 <= 0;
		V_MOD_1_3 <= 0;
	end
	else if (state == RECON && cnt > 0) begin
		if (cnt[0] == 1) begin
			V_MOD_0_0 <= {NTT_DO_MOD[127:112], NTT_DO_MOD[95:80]};
			V_MOD_1_0 <= {NTT_DO_MOD[111:96], NTT_DO_MOD[79:64]};
			V_MOD_0_1 <= {NTT_DO_MOD[63:48], NTT_DO_MOD[31:16]};
			V_MOD_1_1 <= {NTT_DO_MOD[47:32], NTT_DO_MOD[15:0]};
		end
		else if (cnt[0] == 0) begin
			V_MOD_0_2 <= {NTT_DO_MOD[127:112], NTT_DO_MOD[95:80]};
			V_MOD_1_2 <= {NTT_DO_MOD[111:96], NTT_DO_MOD[79:64]};
			V_MOD_0_3 <= {NTT_DO_MOD[63:48], NTT_DO_MOD[31:16]};
			V_MOD_1_3 <= {NTT_DO_MOD[47:32], NTT_DO_MOD[15:0]};
		end
	end
end

always @ (posedge Clk) begin
	if (Reset) begin
		REC_0_W0 <= 0;
		REC_0_W1 <= 0;
		REC_0_W2 <= 0;
		REC_0_W3 <= 0;
		REC_1_W0 <= 0;
		REC_1_W1 <= 0;
		REC_1_W2 <= 0;
		REC_1_W3 <= 0;
	end
	else if (state == RECON && cnt[0] == 1) begin
		REC_0_W0 <= V_MOD_0_0;
		REC_0_W1 <= V_MOD_0_1;
		REC_0_W2 <= V_MOD_0_2;
		REC_0_W3 <= V_MOD_0_3;
		REC_1_W0 <= V_MOD_1_0;
		REC_1_W1 <= V_MOD_1_1;
		REC_1_W2 <= V_MOD_1_2;
		REC_1_W3 <= V_MOD_1_3;
	end
	else if (state == RECON && cnt[0] == 0) begin
		REC_0_W0 <= {REC_0_W0[15:0], 16'd0};
        REC_0_W1 <= {REC_0_W1[15:0], 16'd0};
        REC_0_W2 <= {REC_0_W2[15:0], 16'd0};
        REC_0_W3 <= {REC_0_W3[15:0], 16'd0};
        REC_1_W0 <= {REC_1_W0[15:0], 16'd0};
        REC_1_W1 <= {REC_1_W1[15:0], 16'd0};
        REC_1_W2 <= {REC_1_W2[15:0], 16'd0};
        REC_1_W3 <= {REC_1_W3[15:0], 16'd0};
	end
end

always @ (*) begin
	if (state == RECON && cnt >= 4) begin
		{REC_0_R0, REC_0_R1, REC_0_R2, REC_0_R3} = R[2040-16*(cnt-4)+:8];
		{REC_1_R0, REC_1_R1, REC_1_R2, REC_1_R3} = R[2032-16*(cnt-4)+:8];
	end
	else begin
		{REC_0_R0, REC_0_R1, REC_0_R2, REC_0_R3} = 0;
		{REC_1_R0, REC_1_R1, REC_1_R2, REC_1_R3} = 0;
	end
end

////Address Traverse
always @ (posedge Clk) begin
	if (Reset) begin
		read_addr[0] <= 0;
		read_addr[1] <= 1;
	end
	else begin
		read_addr[0] <= read_addr_next[0];
		read_addr[1] <= read_addr_next[1];
	end
end

always @ (*)
	if (SAVE_S_Fin || COM_B_Fin || RECON_Fin) begin
		read_addr_next[0] = 0;
		read_addr_next[1] = 1;
	end
	else if (COM_US_Fin) begin
		read_addr_next[0] = 0;
		read_addr_next[1] = 64;
	end
	else if (state == SAVE_S && NTT_Cmd == 0) begin
		read_addr_next[0] = read_addr[0] + 2;
		read_addr_next[1] = read_addr[1] + 2;
	end
	else if (state == COM_B && (cnt_coef >= 8)) begin
		read_addr_next[0] = read_addr[0] + 2;
		read_addr_next[1] = read_addr[1] + 2;
	end
	else if (state == COM_US) begin
		read_addr_next[0] = read_addr[0] + 2;
		read_addr_next[1] = read_addr[1] + 2;
	end
	else if (state == RECON) begin
		if (read_addr[0] < 128) begin
			read_addr_next[0] = read_addr[0] + 128;
			read_addr_next[1] = read_addr[1] + 128;
		end
		else begin
			read_addr_next[0] = read_addr[0] - 127;
			read_addr_next[1] = read_addr[1] - 127;
		end
	end
	else begin
		read_addr_next[0] = read_addr[0];
		read_addr_next[1] = read_addr[1];
	end

always @ (posedge Clk)
	if (Reset)
		begin
		write_addr[0] <= 0;
		write_addr[1] <= 1;
		end
	else
		begin
		write_addr[0] <= write_addr_next[0];
		write_addr[1] <= write_addr_next[1];
		end

always @ (*)
	if (GEN_S_Fin || SAVE_S_Fin || GEN_E_Fin || COM_B_Fin || COM_US_Fin)
		begin
		write_addr_next[0] = 0;
		write_addr_next[1] = 1;
		end
	else if (NTT_Cmd == 1 || s_we == 1'b1)
		begin
		write_addr_next[0] = write_addr[0] + 2;
		write_addr_next[1] = write_addr[1] + 2;
		end
	else if (state == COM_B && u_we == 1)
		begin
		write_addr_next[0] = write_addr[0] + 2;
		write_addr_next[1] = write_addr[1] + 2;
		end
	else
		begin
		write_addr_next[0] = write_addr[0];
		write_addr_next[1] = write_addr[1];
		end
		
endmodule
