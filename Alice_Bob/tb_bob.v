`timescale 1ns/100ps

module tb_bob;

reg					Clk;

initial begin
	Clk = 1;
end

always #1 Clk <= ~Clk;

initial begin
		$dumpfile("tb_bob.vcd");
		$dumpvars;
end

reg							Reset;
reg 			[2 : 0] 			Cmd;
reg			[7 : 0] 			Addr;
reg			[63 : 0]			Data_In;
wire			[63 : 0]			Data_Out;
wire							Valid; 

integer							i;
integer							j;
integer							file_sb;
integer							file_sb_hat;
integer							file_eb;
integer							file_e2;
integer							file_a_hat;
integer							file_ub_hat;
integer							file_R;
integer							file_vb;

reg			[15 : 0]			mem_b_hat		[0 : 1023];
reg			[255 : 0]			mem_seed		[0 : 0];
reg			[2047 : 0]			R;
reg			[255 : 0]			Share_Secret;

wire			[255 : 0]			Answer = 256'h1d2c5e1f47d7c45d7179a2dd161464c9e819b5eb43b28731471dcba38c96f826;

initial begin

	#20;
	Reset = 1;
	Cmd = 0;
	#20;
	Reset = 0;
	#20;
	Cmd = 1;

	//cmd = 2;
	wait(BOB_0.state == 1); // INPUT_B starts.
	$readmemh("b_hat.txt", mem_b_hat);
	for (i=0; i<256; i=i+1) begin
		#0.1;
		Cmd = 2;
		Addr = i;
		Data_In = {mem_b_hat[4*i+0], mem_b_hat[4*i+1], mem_b_hat[4*i+2], mem_b_hat[4*i+3]};
		#1.9;
	end
	#0.1;
	$readmemb("seed.txt", mem_seed);
	for (i=0; i<4; i=i+1) begin
		#0.1;
		Cmd = 3;
		Addr = i;
		Data_In = mem_seed[0][64*i+:64];
		#1.9;
	end
	Cmd = 4;
	
	
	wait(BOB_0.state == 2); // GEN_S starts.
	file_sb = $fopen("sb.txt", "w");
	for (i=0; i<256; i=i+1) begin
		wait(BOB_0.sampling_in_ready && BOB_0.sampling_out_ready_bi) #0.1;
		$fwrite(file_sb, "%d\n%d\n%d\n%d\n", BOB_0.sampling_out_bi_3, BOB_0.sampling_out_bi_2, BOB_0.sampling_out_bi_1, BOB_0.sampling_out_bi_0);
		#2;
	end
	$fclose(file_sb);
	$display("File sb.txt dumped.");

	wait(BOB_0.state == 4); // SAVE_S starts.
	file_sb_hat = $fopen("sb_hat.txt", "w");
	wait(BOB_0.s_we) #0.1;
	for (i=0; i<128; i=i+1) begin
		for (j=0; j<4; j=j+1) begin
			$fwrite(file_sb_hat, "%d\n", BOB_0.s_di[0][16*(3-j)+:16]);
		end
		for (j=0; j<4; j=j+1) begin
			$fwrite(file_sb_hat, "%d\n", BOB_0.s_di[1][16*(3-j)+:16]);
		end
		#2;
	end
	$fclose(file_sb_hat);
	$display("File sb_hat.txt dumped.");

	wait(BOB_0.state == 7); // COM_V starts.
	file_vb = $fopen("vb.txt", "w");
	wait(BOB_0.u_we) #0.1;
	for (i=0; i<128; i=i+1) begin
		for (j=0; j<4; j=j+1) begin
			$fwrite(file_vb, "%d\n", BOB_0.u_di[0][16*(3-j)+:16]);
		end
		for (j=0; j<4; j=j+1) begin
			$fwrite(file_vb, "%d\n", BOB_0.u_di[1][16*(3-j)+:16]);
		end
		#2;
	end
	$fclose(file_vb);
	$display("File vb.txt dumped.");
/*
	file_e2 = $fopen("e2.txt","w");
	for (i=0; i<256; i=i+1) begin
		wait(BOB_0.sampling_in_ready && BOB_0.sampling_out_ready_bi) #0.1;
		$fwrite(file_e2, "%d\n%d\n%d\n%d\n", BOB_0.sampling_out_bi_3, BOB_0.sampling_out_bi_2, BOB_0.sampling_out_bi_1, BOB_0.sampling_out_bi_0);
		#2;
	end
	$fclose(file_e2);
	$display("File e2.txt dumped.");
*/	

	wait(BOB_0.state == 9); // GEN_E starts.
	file_eb = $fopen("eb.txt","w");
	for (i=0; i<256; i=i+1) begin
		wait(BOB_0.sampling_in_ready && BOB_0.sampling_out_ready_bi) #0.1;
		$fwrite(file_eb, "%d\n%d\n%d\n%d\n", BOB_0.sampling_out_bi_3, BOB_0.sampling_out_bi_2, BOB_0.sampling_out_bi_1, BOB_0.sampling_out_bi_0);
		#2;
	end
	$fclose(file_eb);
	$display("File eb.txt dumped.");
/*
	wait(BOB_0.state == 7); // COM_U starts.
	file_a_hat = $fopen("a_hat.txt", "w");
	for (i=0; i<128; i=i+1) begin
		wait(BOB_0.cnt_coef >= 8) #0.1;
		$fwrite(file_a_hat, "%d\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n", BOB_0.Gen_Coef[127:112], BOB_0.Gen_Coef[111:96], BOB_0.Gen_Coef[95:80], BOB_0.Gen_Coef[79:64], BOB_0.Gen_Coef[63:48], BOB_0.Gen_Coef[47:32], BOB_0.Gen_Coef[31:16], BOB_0.Gen_Coef[15:0]);
		#2;
	end
	$fclose(file_a_hat);
	$display("File a_hat.txt dumped.");

*/
	wait(Valid); // Done
	file_R = $fopen("R.txt","w");
	for (i=31; i>=0; i=i-1) begin
		Cmd = 5;	//read R
		Addr = i;
		#2;
		//R[64*i+:64] = Data_Out;
		$fwrite(file_R, "%b\n%b\n%b\n%b\n%b\n%b\n%b\n%b\n", Data_Out[63:56], Data_Out[55:48], Data_Out[47:40], Data_Out[39:32], Data_Out[31:24], Data_Out[23:16], Data_Out[15:8], Data_Out[7:0]);
	end
	//$fwrite(file_R, "%d", R);
	$fclose(file_R);
	$display("File R.txt dumped.");
	
	Cmd = 0;	
	file_ub_hat = $fopen("ub_hat.txt", "w");
	for (i=0; i<256; i=i+1) begin
		Addr = i;
		#2.1;
		$fwrite(file_ub_hat, "%h\n%h\n%h\n%h\n", BOB_0.Data_Out[63:48], BOB_0.Data_Out[47:32], BOB_0.Data_Out[31:16], BOB_0.Data_Out[15:0]);
		#1.9;
	end
	$fclose(file_ub_hat);
	$display("File ub_hat.txt dumped.");
/*	
	#0.1;
	for (i=0; i<4; i=i+1) begin
		Cmd = 6;
		Addr = i;
		#2;
		Seed[64*i+:64] = Data_Out;
	end
	#1.9;
	
	file_seed = $fopen("seed.txt", "w");
	$fwrite(file_seed, "%d", Seed);
	$fclose(file_seed);
	$display("File seed.txt dumped.");
*/
	#0.1;
	for (i=0; i<4; i=i+1) begin
		Cmd = 6;
		Addr = i;
		#2;
		Share_Secret[64*i+:64] = Data_Out;
	end
	#1.9;
	$display("Key = %h. (Computed)", Share_Secret);
	$display("Key = %h. (Expected)", Answer);
	if (Share_Secret == Answer) begin
		$display("(O)");
	end
	else begin
		$display("(X)");
	end
	
	#20;
	$finish;

end

bob BOB_0 (
	.Clk(Clk),
	.Reset(Reset),
	.Cmd(Cmd),
	.Addr(Addr),
	.Data_In(Data_In),
	.Data_Out(Data_Out),
	.Valid(Valid)
    );

endmodule
