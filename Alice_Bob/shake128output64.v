/*
 * Copyright 2013, Homer Hsing <homer.hsing@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* if "ack" is 1, then current input has been used. */

`define low_pos(w,b)      ((w)*64 + (b)*8)
`define low_pos2(w,b)     `low_pos(w,7-b)
`define high_pos(w,b)     (`low_pos(w,b) + 7)
`define high_pos2(w,b)    (`low_pos2(w,b) + 7)

module shake128output64(clk, reset, in, in_ready, next, ack, out, out_ready, cnt , state);
    input               clk, reset;
    input      [1343:0] in;
    input               in_ready;
    input               next;
    output reg          ack;
    output      [63:0]  out;
    output reg          out_ready;

    
    output reg [1599:0] state;
    reg        [22:0]   i; /* select round constant */
    reg        [1343:0] out_buffer;
    wire       [1599:0] round_in, round_out;
    wire       [63:0]   rc; /* round constant */
    wire                update;
    wire                accept;
    reg                 calc; /* == 1: calculating rounds */
    output reg  [4:0]   cnt;
    wire                need;
    reg                 state_ready;
    genvar w, b;
    wire        [1343:0]reordered_out;

    assign start = (in_ready | (need & ack)) & (~calc);
    
    always @ (posedge clk)
      if (reset) i <= 0;
      else       i <= {i[21:0], start};
    
    always @ (posedge clk)
      if (reset) calc <= 0;
      else       calc <= (calc & (~ i[22])) | start;

    assign update = calc | start;

    always @ (posedge clk)
      if(reset)
      begin
        state_ready <= 0;
      end
      else if (start)
        state_ready <= 0;
      else if (i[22]) // only change at the last round
        state_ready <= 1;
      
	assign need = (reset == 1) || (cnt == 0 && state_ready == 1);
    always @ (posedge clk)
      if(reset)
      begin
        //need<=1;
        cnt <= 0;
        out_ready <=0;
      end
      else if (next)
      begin
        if(cnt!=0)
        begin
          out_buffer <= {out_buffer[1343-64:0],64'h0};
          cnt <= cnt - 1;
          out_ready <= 1;
          //need <= 0;
        end
        else if(cnt==0&state_ready)
        begin
          //out_buffer = state[1599:1599-1343];
          out_buffer <= reordered_out;
          //need <= 1;
          cnt <= 8'd20;
          out_ready <= 1;
        end
        else
          out_ready <= 0;
      end

    assign accept = (in_ready & (~calc));
    //assign ack = in_ready;

    rconst
      rconst_ ({i, accept}, rc);

    round
      round_ (round_in, rc, round_out);

    always @ (posedge clk)
      if(reset)
        ack <= 0;
      else
        ack <= accept | ack; // in_ready & (i == 0)
        
    assign round_in = accept ? {in  ^ state[1599:1599-1343], state[1599-1344:0]} : state;


    generate
      for(w=0; w<21; w=w+1)
        begin : L0
          for(b=0; b<8; b=b+1)
            begin : L1
              assign reordered_out[`high_pos(w,b):`low_pos(w,b)] = state[`high_pos2(w,b)+256:`low_pos2(w,b)+256];
            end
        end
    endgenerate
    assign out = out_buffer[1343:1343-63];

    always @ (posedge clk)
      if (reset)
        state <= 0;
      else if (update)
        state <= round_out;
endmodule

`undef low_pos
`undef low_pos2
`undef high_pos
`undef high_pos2
