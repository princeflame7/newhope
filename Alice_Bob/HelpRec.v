module HelpRec (
	clk,
	b,
	w0,
	w1,
	w2,
	w3,
	r0,
	r1,
	r2,
	r3,
	w0_,
	w1_,
	w2_,
	w3_
);

input						clk;
input						b;
input		[13:0]			w0;
input		[13:0]			w1;
input		[13:0]			w2;
input		[13:0]			w3;
output		[1:0]			r0;
output		[1:0]			r1;
output		[1:0]			r2;
output		[1:0]			r3;
output		[13:0]			w0_;
output		[13:0]			w1_;
output		[13:0]			w2_;
output		[13:0]			w3_;

wire		[15:0]			x0q;
wire		[15:0]			x1q;
wire		[15:0]			x2q;
wire		[15:0]			x3q;

wire		[2:0]			v00;
wire		[2:0]			v01;
wire		[2:0]			v02;
wire		[2:0]			v03;
wire		[1:0]			v10;
wire		[1:0]			v11;
wire		[1:0]			v12;
wire		[1:0]			v13;

wire		[15:0]			y0;
wire		[15:0]			y1;
wire		[15:0]			y2;
wire		[15:0]			y3;

wire						k;

wire		[1:0]			v0;
wire		[1:0]			v1;
wire		[1:0]			v2;
wire		[1:0]			v3;

wire		[1:0]			r0_;
wire		[1:0]			r1_;
wire		[1:0]			r2_;
wire		[1:0]			r3_;

reg		[15:0]			X0Q;
reg		[15:0]			X1Q;
reg		[15:0]			X2Q;
reg		[15:0]			X3Q;
reg		[13:0]			W0;
reg		[13:0]			W1;
reg		[13:0]			W2;
reg		[13:0]			W3;

reg		[15:0]			X0Q_;
reg		[15:0]			X1Q_;
reg		[15:0]			X2Q_;
reg		[15:0]			X3Q_;
reg		[2:0]			V00;
reg		[2:0]			V01;
reg		[2:0]			V02;
reg		[2:0]			V03;
reg		[1:0]			V10;
reg		[1:0]			V11;
reg		[1:0]			V12;
reg		[1:0]			V13;
reg		[13:0]			W0_;
reg		[13:0]			W1_;
reg		[13:0]			W2_;
reg		[13:0]			W3_;

reg		[2:0]			V00_;
reg		[2:0]			V01_;
reg		[2:0]			V02_;
reg		[2:0]			V03_;
reg		[1:0]			V10_;
reg		[1:0]			V11_;
reg		[1:0]			V12_;
reg		[1:0]			V13_;
reg		[15:0]			Y0;
reg		[15:0]			Y1;
reg		[15:0]			Y2;
reg		[15:0]			Y3;
reg		[13:0]			W0__;
reg		[13:0]			W1__;
reg		[13:0]			W2__;
reg		[13:0]			W3__;

reg		[2:0]			V00__;
reg		[2:0]			V01__;
reg		[2:0]			V02__;
reg		[2:0]			V03__;
reg		[1:0]			V10__;
reg		[1:0]			V11__;
reg		[1:0]			V12__;
reg		[1:0]			V13__;
reg						K;
reg		[13:0]			W0___;
reg		[13:0]			W1___;
reg		[13:0]			W2___;
reg		[13:0]			W3___;

reg						K_;
reg		[1:0]			V0;
reg		[1:0]			V1;
reg		[1:0]			V2;
reg		[1:0]			V3;
reg		[13:0]			W0____;
reg		[13:0]			W1____;
reg		[13:0]			W2____;
reg		[13:0]			W3____;

reg		[1:0]			R0_;
reg		[1:0]			R1_;
reg		[1:0]			R2_;
reg		[1:0]			R3_;
reg		[13:0]			W0_____;
reg		[13:0]			W1_____;
reg		[13:0]			W2_____;
reg		[13:0]			W3_____;

assign x0q = {w0, b, 1'b0};
assign x1q = {w1, b, 1'b0};
assign x2q = {w2, b, 1'b0};
assign x3q = {w3, b, 1'b0};

assign v00 = (X0Q<=6144) ? 3'd0 : (X0Q<=18433) ? 3'd1 : (X0Q<=30722) ? 3'd2 : (X0Q<=43011) ? 3'd3 : 3'd4;
assign v01 = (X1Q<=6144) ? 3'd0 : (X1Q<=18433) ? 3'd1 : (X1Q<=30722) ? 3'd2 : (X1Q<=43011) ? 3'd3 : 3'd4;
assign v02 = (X2Q<=6144) ? 3'd0 : (X2Q<=18433) ? 3'd1 : (X2Q<=30722) ? 3'd2 : (X2Q<=43011) ? 3'd3 : 3'd4;
assign v03 = (X3Q<=6144) ? 3'd0 : (X3Q<=18433) ? 3'd1 : (X3Q<=30722) ? 3'd2 : (X3Q<=43011) ? 3'd3 : 3'd4;
assign v10 = (X0Q<=12288) ? 2'd0 : (X0Q<=24577) ? 2'd1 : (X0Q<=36866) ? 2'd2 : 2'd3; 
assign v11 = (X1Q<=12288) ? 2'd0 : (X1Q<=24577) ? 2'd1 : (X1Q<=36866) ? 2'd2 : 2'd3; 
assign v12 = (X2Q<=12288) ? 2'd0 : (X2Q<=24577) ? 2'd1 : (X2Q<=36866) ? 2'd2 : 2'd3; 
assign v13 = (X3Q<=12288) ? 2'd0 : (X3Q<=24577) ? 2'd1 : (X3Q<=36866) ? 2'd2 : 2'd3; 

assign y0 = (X0Q_>=V00*12289) ? X0Q_-V00*12289 : V00*12289-X0Q_;
assign y1 = (X1Q_>=V01*12289) ? X1Q_-V01*12289 : V01*12289-X1Q_;
assign y2 = (X2Q_>=V02*12289) ? X2Q_-V02*12289 : V02*12289-X2Q_;
assign y3 = (X3Q_>=V03*12289) ? X3Q_-V03*12289 : V03*12289-X3Q_;

assign k = (Y0+Y1+Y2+Y3<12289) ? 1'b0 : 1'b1;

assign v0 = (K==0) ? V00__ : V10__;
assign v1 = (K==0) ? V01__ : V11__;
assign v2 = (K==0) ? V02__ : V12__;
assign v3 = (K==0) ? V03__ : V13__;

assign r0_ = V0 - V3;
assign r1_ = V1 - V3;
assign r2_ = V2 - V3;
assign r3_ = K_ + 2*V3;

assign r0 = R0_;
assign r1 = R1_;
assign r2 = R2_;
assign r3 = R3_;
assign w0_ = W0_____;
assign w1_ = W1_____;
assign w2_ = W2_____;
assign w3_ = W3_____;

always @ (posedge clk) begin
	X0Q <= x0q;
	X1Q <= x1q;
	X2Q <= x2q;
	X3Q <= x3q;
	W0 <= w0;
	W1 <= w1;
	W2 <= w2;
	W3 <= w3;

	X0Q_ <= X0Q;
	X1Q_ <= X1Q;
	X2Q_ <= X2Q;
	X3Q_ <= X3Q;
	V00 <= v00;
	V01 <= v01;
	V02 <= v02;
	V03 <= v03;
	V10 <= v10;
	V11 <= v11;
	V12 <= v12;
	V13 <= v13;
	W0_ <= W0;
	W1_ <= W1;
	W2_ <= W2;
	W3_ <= W3;
	
	V00_ <= V00;
	V01_ <= V01;
	V02_ <= V02;
	V03_ <= V03;
	V10_ <= V10;
	V11_ <= V11;
	V12_ <= V12;
	V13_ <= V13;
	Y0 <= y0;
	Y1 <= y1;
	Y2 <= y2;
	Y3 <= y3;
	W0__ <= W0_;
	W1__ <= W1_;
	W2__ <= W2_;
	W3__ <= W3_;
	
	V00__ <= V00_;
	V01__ <= V01_;
	V02__ <= V02_;
	V03__ <= V03_;
	V10__ <= V10_;
	V11__ <= V11_;
	V12__ <= V12_;
	V13__ <= V13_;
	K <= k;
	W0___ <= W0__;
	W1___ <= W1__;
	W2___ <= W2__;
	W3___ <= W3__;
	
	K_ <= K;
	V0 <= v0;
	V1 <= v1;
	V2 <= v2;
	V3 <= v3;
	W0____ <= W0___;
	W1____ <= W1___;
	W2____ <= W2___;
	W3____ <= W3___;

	R0_ <= r0_;
	R1_ <= r1_;
	R2_ <= r2_;
	R3_ <= r3_;
	W0_____ <= W0____;
	W1_____ <= W1____;
	W2_____ <= W2____;
	W3_____ <= W3____;
end

endmodule
