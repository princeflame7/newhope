`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    
// Design Name: 
// Module Name:    BFU12289_kred (v2)
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description:  Remove enable, constantly outputting value outside module needs
//				 to calculate cycle to retrieve output value
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module BFU12289_kred(
	Clk,
	Resetn,
	a0,
	a1,
	w,
	A0,
	A1
    );

parameter P_WIDTH = 16;
parameter W_WIDTH = 14;
input							Clk;
input							Resetn;
input		[P_WIDTH-1 : 0]		a0;
input		[P_WIDTH-1 : 0]		a1;
input		[W_WIDTH-1 : 0]		w;
output		[P_WIDTH-1 : 0]		A0;
output		[P_WIDTH-1 : 0]		A1;


reg			[P_WIDTH-1 : 0]		m0;
reg			[P_WIDTH+W_WIDTH-1 : 0] 	m1;
reg			[3 : 0]				cnt;

//valid at clock = 4

//clk: 1: m0, m1 stable; 2 tU0, nV0 .... stable; 3 nV0mtV1, tU0mU1pV2... stable; 4:outUpV outUmV stable
//En really not necessary may need to remove
always @ (posedge Clk)
	if (Resetn == 1'b0)
	begin
	m0 <= 0;
	m1 <= 0;
	end
	else
	begin////Additional pipeline stage
	m0 <= a0;
	m1 <= a1 * w; 
	end
	
kredUV mod( .clk(Clk), .resetn(Resetn), .inU(m0), .inV({2'd0,m1}), .outUpV(A0), .outUmV(A1) );


endmodule
