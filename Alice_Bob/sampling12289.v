module sampling12289(clk, reset, in, in_ready, need ,next, out, out_ready);

    input               clk, reset;
    input        [15:0] in;
    input               in_ready;
    input               next;
    output reg   [15:0] out;
    output reg          out_ready;
    output wire			need;

	assign need = next;

	always @ (posedge clk) begin
		if (reset) begin
			out_ready <= 0;
			out <= 0;
		end
		else if ((next == 1) && (in_ready == 1) && (in<12289*5)) begin
			out <= in;
			out_ready <= 1;
		end
		else begin
			out <= in;
			out_ready <= 0;
		end
	end

endmodule
