module Rec (
	clk,
	w0,
	w1,
	w2,
	w3,
	r0,
	r1,
	r2,
	r3,
	secret
);

input						clk;
input		[13:0]			w0;
input		[13:0]			w1;
input		[13:0]			w2;
input		[13:0]			w3;
input		[1:0]			r0;
input		[1:0]			r1;
input		[1:0]			r2;
input		[1:0]			r3;
output						secret;

wire		[17:0]			x08q;
wire		[17:0]			x18q;
wire		[17:0]			x28q;
wire		[17:0]			x38q;
wire		[16:0]			v08q;
wire		[16:0]			v18q;
wire		[16:0]			v28q;
wire		[16:0]			v38q;
wire						prev_secret;

reg			[17:0]			X08Q;
reg			[17:0]			X18Q;
reg			[17:0]			X28Q;
reg			[17:0]			X38Q;
reg			[16:0]			V08Q;
reg			[16:0]			V18Q;
reg			[16:0]			V28Q;
reg			[16:0]			V38Q;
reg							SECRET;

assign x08q = (8*w0>=24578*r0+12289*r3) ? 8*w0-24578*r0-12289*r3 : 24578*r0+12289*r3-8*w0;
assign x18q = (8*w1>=24578*r1+12289*r3) ? 8*w1-24578*r1-12289*r3 : 24578*r1+12289*r3-8*w1;
assign x28q = (8*w2>=24578*r2+12289*r3) ? 8*w2-24578*r2-12289*r3 : 24578*r2+12289*r3-8*w2;
assign x38q = (8*w3>=12289*r3) ? 8*w3-12289*r3 : 12289*r3-8*w3;
assign v08q = (X08Q<49156) ? X08Q : (X08Q<98312) ? 98312-X08Q : X08Q-98312;
assign v18q = (X18Q<49156) ? X18Q : (X18Q<98312) ? 98312-X18Q : X18Q-98312;
assign v28q = (X28Q<49156) ? X28Q : (X28Q<98312) ? 98312-X28Q : X28Q-98312;
assign v38q = (X38Q<49156) ? X38Q : (X38Q<98312) ? 98312-X38Q : X38Q-98312;
assign prev_secret = (V08Q+V18Q+V28Q+V38Q<=98312) ? 1'b0 : 1'b1;
assign secret = SECRET;

always @ (posedge clk) begin
	X08Q <= x08q;
	X18Q <= x18q;
	X28Q <= x28q;
	X38Q <= x38q;
	V08Q <= v08q;
	V18Q <= v18q;
	V28Q <= v28q;
	V38Q <= v38q;
	SECRET <= prev_secret;
end

endmodule
