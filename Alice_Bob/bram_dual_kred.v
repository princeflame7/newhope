`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:47:32 03/17/2017 
// Design Name: 
// Module Name:    bram_dual_kred 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module bram_dual_kred(
	Clk, En, We, Addr_A, Addr_B, DI_A, DI_B, DO_A, DO_B
    );
parameter WIDTH = 64;

input 		Clk;
input 		En;
input 		We;
input 		[7 : 0]			Addr_A;
input 		[7 : 0]			Addr_B;
input 		[WIDTH - 1 : 0]	DI_A;
input 		[WIDTH - 1 : 0]	DI_B;
output 		[WIDTH - 1 : 0]	DO_A;
output 		[WIDTH - 1 : 0]	DO_B;

reg 		[WIDTH - 1 : 0]	ram 	 [0 : 255];
reg	 		[WIDTH - 1 : 0]	DO_A;
reg 	 	[WIDTH - 1 : 0]	DO_B;

integer 	i;


always @(posedge Clk) 
	begin
	if (En)
		begin
		if (We)
			ram[Addr_A] <= DI_A;
		DO_A <= ram[Addr_A];
		end 
	end
	
always @(posedge Clk) 
	begin
	if (En)
		begin
		if (We)
			ram[Addr_B] <= DI_B;
		DO_B <= ram[Addr_B];
		end 
	end

endmodule
