`timescale 1ns/100ps

module tb_alice;

reg					Clk;

initial begin
	Clk = 1;
end

always #1 Clk <= ~Clk;

initial begin
		$dumpfile("tb_alice.vcd");
		$dumpvars;
end

reg								Reset;
reg 		[2 	: 0] 			Cmd;
reg			[7 : 0] 			Addr;
reg			[63 : 0]			Data_In;
wire		[63 : 0]			Data_Out;
wire							Valid; 

integer							i;
integer							j;
integer							file_s;
integer							file_s_hat;
integer							file_e;
integer							file_a_hat;
integer							file_b_hat;
integer							file_seed;
integer							file_va_hat;

reg			[15 : 0]			mem_u_hat		[0 : 1023];
reg			[7 : 0]				mem_r			[0 : 255];
reg			[255 : 0]			Seed;
reg			[255 : 0]			Share_Secret;

wire		[255 : 0]			Answer = 256'h1d2c5e1f47d7c45d7179a2dd161464c9e819b5eb43b28731471dcba38c96f826;

initial begin

	#20;
	Reset = 1;
	Cmd = 0;
	#20;
	Reset = 0;
	#20;
	Cmd = 1;

	wait(ALICE_0.state == 1); // GEN_S starts.
	file_s = $fopen("s.txt", "w");
	for (i=0; i<256; i=i+1) begin
		wait(ALICE_0.sampling_in_ready && ALICE_0.sampling_out_ready_bi) #0.1;
		$fwrite(file_s, "%d\n%d\n%d\n%d\n", ALICE_0.sampling_out_bi_3, ALICE_0.sampling_out_bi_2, ALICE_0.sampling_out_bi_1, ALICE_0.sampling_out_bi_0);
		#2;
	end
	$fclose(file_s);
	$display("File s.txt dumped.");

	wait(ALICE_0.state == 3); // SAVE_S starts.
	file_s_hat = $fopen("s_hat.txt", "w");
	wait(ALICE_0.s_we) #0.1;
	for (i=0; i<128; i=i+1) begin
		for (j=0; j<4; j=j+1) begin
			$fwrite(file_s_hat, "%d\n", ALICE_0.s_di[0][16*(3-j)+:16]);
		end
		for (j=0; j<4; j=j+1) begin
			$fwrite(file_s_hat, "%d\n", ALICE_0.s_di[1][16*(3-j)+:16]);
		end
		#2;
	end
	$fclose(file_s_hat);
	$display("File s_hat.txt dumped.");
	
	wait(ALICE_0.state == 4); // GEN_E starts.
	file_e = $fopen("e.txt","w");
	for (i=0; i<256; i=i+1) begin
		wait(ALICE_0.sampling_in_ready && ALICE_0.sampling_out_ready_bi) #0.1;
		$fwrite(file_e, "%d\n%d\n%d\n%d\n", ALICE_0.sampling_out_bi_3, ALICE_0.sampling_out_bi_2, ALICE_0.sampling_out_bi_1, ALICE_0.sampling_out_bi_0);
		#2;
	end
	$fclose(file_e);
	$display("File e.txt dumped.");

	wait(ALICE_0.state == 6); // COM_B starts.
	file_a_hat = $fopen("a_hat.txt", "w");
	for (i=0; i<128; i=i+1) begin
		wait(ALICE_0.cnt_coef >= 8) #0.1;
		$fwrite(file_e, "%d\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n", ALICE_0.Gen_Coef[127:112], ALICE_0.Gen_Coef[111:96], ALICE_0.Gen_Coef[95:80], ALICE_0.Gen_Coef[79:64], ALICE_0.Gen_Coef[63:48], ALICE_0.Gen_Coef[47:32], ALICE_0.Gen_Coef[31:16], ALICE_0.Gen_Coef[15:0]);
		#2;
	end
	$fclose(file_a_hat);
	$display("File a_hat.txt dumped.");

	wait(ALICE_0.state == 7); // WAIT_U starts.
	file_b_hat = $fopen("b_hat.txt", "w");
	for (i=0; i<256; i=i+1) begin
		Addr = i;
		#2.1;
		$fwrite(file_b_hat, "%h\n%h\n%h\n%h\n", ALICE_0.Data_Out[63:48], ALICE_0.Data_Out[47:32], ALICE_0.Data_Out[31:16], ALICE_0.Data_Out[15:0]);
		#1.9;
	end
	$fclose(file_b_hat);
	$display("File b_hat.txt dumped.");
	#0.1;
	for (i=0; i<4; i=i+1) begin
		Cmd = 6;
		Addr = i;
		#2;
		Seed[64*i+:64] = Data_Out;
	end
	#1.9;
	file_seed = $fopen("seed.txt", "w");
	$fwrite(file_seed, "%d", Seed);
	$fclose(file_seed);
	$display("File seed.txt dumped.");
	#0.1;
	Cmd = 2;

	wait(ALICE_0.state == 8); // INPUT_U starts.
	$readmemh("u_hat.txt", mem_u_hat);
	for (i=0; i<256; i=i+1) begin
		#0.1;
		Cmd = 3;
		Addr = i;
		Data_In = {mem_u_hat[4*i+0], mem_u_hat[4*i+1], mem_u_hat[4*i+2], mem_u_hat[4*i+3]};
		#1.9;
	end
	#0.1;
	$readmemb("r.txt", mem_r);
	for (i=0; i<32; i=i+1) begin
		#0.1;
		Cmd = 5;
		Addr = 31 - i;
		Data_In = {mem_r[8*i+0], mem_r[8*i+1], mem_r[8*i+2], mem_r[8*i+3], mem_r[8*i+4], mem_r[8*i+5], mem_r[8*i+6], mem_r[8*i+7]};
		#1.9;
	end
	Cmd = 4;

	wait(ALICE_0.state == 9); // COM_US starts.
	file_va_hat = $fopen("va_hat.txt", "w");
	for (i=0; i<128; i=i+1) begin
		wait(ALICE_0.NTT_Cmd == 1) #0.1;
		$fwrite(file_va_hat, "%d\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n", ALICE_0.NTT_DI[127:112], ALICE_0.NTT_DI[111:96], ALICE_0.NTT_DI[95:80], ALICE_0.NTT_DI[79:64], ALICE_0.NTT_DI[63:48], ALICE_0.NTT_DI[47:32], ALICE_0.NTT_DI[31:16], ALICE_0.NTT_DI[15:0]);
		#1.9;
	end
	$fclose(file_va_hat);
	$display("File va_hat.txt dumped.");

	wait(Valid);
	#0.1;
	for (i=0; i<4; i=i+1) begin
		Cmd = 7;
		Addr = i;
		#2;
		Share_Secret[64*i+:64] = Data_Out;
	end
	#1.9;
	$display("Key = %h. (Computed)", Share_Secret);
	$display("Key = %h. (Expected)", Answer);
	if (Share_Secret == Answer) begin
		$display("(O)");
	end
	else begin
		$display("(X)");
	end
	
	#20;
	$finish;

end

alice ALICE_0 (
	.Clk(Clk),
	.Reset(Reset),
	.Cmd(Cmd),
	.Addr(Addr),
	.Data_In(Data_In),
	.Data_Out(Data_Out),
	.Valid(Valid)
    );

endmodule
