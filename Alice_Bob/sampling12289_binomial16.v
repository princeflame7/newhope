module sampling12289_binomial16(clk, reset, in, in_ready, need ,next, out, out_ready);
    input               clk, reset;
    input        [15:0] in;
    input               in_ready;
    input               next;
    output reg   [13:0] out;
    output reg          out_ready;
    output wire		need;
    wire           [3:0] hamming_weight;
    reg           [1:0] i;
    wire         [13:0] tmp;


    assign need = next;

    assign hamming_weight = in[0] + in[1] + in[2] + in[3] + in[4] + in[5] + in[6] + in[7] + in[8] + in[9] + in[10] + in[11] + in[12] + in[13] + in[14] + in[15];
    assign tmp = out - hamming_weight;

    always @ (posedge clk)
        if(reset)
        begin
            out_ready <= 0;
            i <= 0;
            out <= 0;
        end
        else if(next)
            if(in_ready)
	    begin
	        if(i==0)
   		begin
	   	    out <= hamming_weight;
       	            i <= 1;
       	            out_ready <= 0;
   	        end
  	        else
  	        begin
   		    out <= tmp[13]? tmp+14'd12289:tmp;
       	            out_ready <= 1;
       	            i <= 0;
       	        end
	    end
endmodule
