`timescale 1ns / 1ps

module TRNG (
	clk,
	TRN
);

input 				clk;
output 	[255:0] 	TRN;

wire	[255:0]		W0;
wire	[255:0]		W1;
wire	[255:0]		W2;
wire	       		W;
reg		[255:0]		TRN;
reg     [1:0]       counter = 0;
wire                clkdiv;

assign W1 = ~W0;
assign W2 = ~W1;
assign W0 = ~W2;
assign W = ^(W2[255:0]);
always @ (posedge clk) begin
    counter <= counter + 1'b1;
end

assign clkdiv = counter[1];

always @ (posedge clkdiv) begin
	//TRN <= {TRN[254:0], W}; 
	TRN <= 0; // Just for Simulation
end

endmodule
