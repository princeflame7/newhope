`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    
// Design Name: 
// Module Name:    BFUset_8_mod12289_v2
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments:  This module implements a single stage of FFT computation (03/10/17)
//						 Order Reverse  have to be done before stage 0.
//						 Remove enable, constantly outputting value outside module needs (03/29/17)
//				 		 to calculate cycle to retrieve output value
//
//////////////////////////////////////////////////////////////////////////////////
module BFUset_8_mod12289(
	Clk,
	Resetn,
	a0, a1, a2, a3, a4, a5, a6, a7,
	w0, w1, w2, w3,
	A0, A1, A2, A3, A4, A5, A6, A7
    );
parameter P_WIDTH = 16;
parameter W_WIDTH = 14;

input 							Clk;
input 							Resetn;

input		[P_WIDTH-1 : 0]		a0;
input		[P_WIDTH-1 : 0]		a1;
input		[P_WIDTH-1 : 0]		a2;
input		[P_WIDTH-1 : 0]		a3;
input		[P_WIDTH-1 : 0]		a4;
input		[P_WIDTH-1 : 0]		a5;
input		[P_WIDTH-1 : 0]		a6;
input		[P_WIDTH-1 : 0]		a7;
input		[W_WIDTH-1 : 0]		w0;
input		[W_WIDTH-1 : 0]		w1;
input		[W_WIDTH-1 : 0]		w2;
input		[W_WIDTH-1 : 0]		w3;

output		[P_WIDTH-1 : 0]		A0;
output		[P_WIDTH-1 : 0]		A1;
output		[P_WIDTH-1 : 0]		A2;
output		[P_WIDTH-1 : 0]		A3;
output		[P_WIDTH-1 : 0]		A4;
output		[P_WIDTH-1 : 0]		A5;
output		[P_WIDTH-1 : 0]		A6;
output		[P_WIDTH-1 : 0]		A7;


BFU12289_kred u0( .Clk(Clk), .Resetn(Resetn), .a0(a0), .a1(a1), .w(w0), .A0(A0), .A1(A1));
BFU12289_kred u1( .Clk(Clk), .Resetn(Resetn), .a0(a2), .a1(a3), .w(w1), .A0(A2), .A1(A3));
BFU12289_kred u2( .Clk(Clk), .Resetn(Resetn), .a0(a4), .a1(a5), .w(w2), .A0(A4), .A1(A5));
BFU12289_kred u3( .Clk(Clk), .Resetn(Resetn), .a0(a6), .a1(a7), .w(w3), .A0(A6), .A1(A7));
	
endmodule


